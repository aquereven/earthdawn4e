export default class earthdawn4eItem extends Item {
  prepareData() {
    super.prepareData();
    // const itemData = this.data;
    // V10 changes
    const itemData = this;
		// change End
    if (this.type === 'spellmatrix') {
      this._prepareItemData(itemData);
    } else if (this.type === 'thread' && this.isEmbedded === true) {
      this._prepareThreadData(itemData);
    } 
    if (this.type === 'weapon' || this.type === 'armor'){
      this._prepareForgedItems(itemData)
    }
  }

  _prepareItemData(itemData) {
    // const data = itemData.data;
    // V10 changes
    const systemData = itemData.system;
		// change End
    systemData.totalthreads = this.threadCount();
    systemData.totalthreadsneeded = Number(systemData.threadsrequired) + Number(systemData.targetadditionalthreads);
  }
  _prepareThreadData(itemData) {}
  threadCount() {
    let totalThreads;
    // totalThreads = Number(this.data.data.constantthreads) + Number(this.data.data.activethreads);
    // V10 changes
    totalThreads = Number(this.system.constantthreads) + Number(this.system.activethreads);
		// change End
    return totalThreads;
  }

  _prepareForgedItems(itemData) {
    // const data = itemData.data
    // V10 changes
    const systemData = itemData.system
		// change End
    // if (itemData.type === "weapon"){
    // data.damageFinal = Number(data.damagestep) + Number(data.timesForged)
    // V10 changes
    if (itemData.type === "weapon"){
      systemData.damageFinal = Number(systemData.damagestep) + Number(systemData.timesForged)
      systemData.damageTotal = this.getFinalDamage()
		// change End
    }
    else {
      //data.physicalArmorFinal = Number(data.Aphysicalarmor) + Number(data.timesForgedPhysical)
      //data.mysticArmorFinal = Number(data.Amysticarmor) + Number(data.timesForgedMystic)
      // V10 changes
      systemData.physicalArmorFinal = Number(systemData.Aphysicalarmor) + Number(systemData.timesForgedPhysical)
      systemData.mysticArmorFinal = Number(systemData.Amysticarmor) + Number(systemData.timesForgedMystic)
		  // change End
    }
  }

  getFinalDamage(){
    console.log(this)
    console.log(game.actors.get(this.parent._id))
    let actor = game.actors.get(this.parent._id)
    let attribute = this.system.damageattribute + "Step";
    console.log(attribute)
    let attributeStep = actor.getStep(actor.system.attributes.strengthvalue);
    console.log(attributeStep)
    let finalDamage = Number(attributeStep) + this.system.damageFinal
    console.log(finalDamage)
    return finalDamage;
  }
}
