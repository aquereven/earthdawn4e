export default class earthdawn4eCombat extends Combat {
  async startCombat() {
    await this.setupTurns();
    console.log('Custom Combat has started');
    return super.startCombat();
  }

  async rollInitiative(ids, {formula=null, updateTurn=true, messageOptions={}}={}) {

    // Structure input data
    ids = typeof ids === "string" ? [ids] : ids;
    const currentId = this.combatant?.id;
    const chatRollMode = game.settings.get("core", "rollMode");

    // Iterate over Combatants, performing an initiative roll for each
    const updates = [];
    const messages = [];
    for ( let [i, id] of ids.entries() ) {

      // Get Combatant data (non-strictly)
      const combatant = this.combatants.get(id);
      if ( !combatant?.isOwner ) continue;

      // Produce an initiative roll for the Combatant
      const roll = combatant.getInitiativeRoll(formula);
      await roll.evaluate({async: true});
      let InitiativeValue = Math.max(1, roll.total);
      updates.push({_id: id, initiative: InitiativeValue});

      // Construct chat message data
      let messageData = foundry.utils.mergeObject({
        speaker: ChatMessage.getSpeaker({
          actor: combatant.actor,
          token: combatant.token,
          alias: combatant.name
        }),
        flavor: game.i18n.format("COMBAT.RollsInitiative", {name: combatant.name}),
        flags: {"core.initiativeRoll": true},
      }, messageOptions);
      const chatData = await roll.toMessage(messageData, {create: false});

      // If the combatant is hidden, use a private roll unless an alternative rollMode was explicitly requested
      chatData.rollMode = "rollMode" in messageOptions ? messageOptions.rollMode :
        (combatant.hidden ? CONST.DICE_ROLL_MODES.PRIVATE : chatRollMode );

      // Play 1 sound for the whole rolled set
      if ( i > 0 ) chatData.sound = null;
      messages.push(chatData);
    }
    if ( !updates.length ) return this;

    // Update multiple combatants
    await this.updateEmbeddedDocuments("Combatant", updates);

    // Ensure the turn order remains with the same combatant
    if ( updateTurn && currentId ) {
      await this.update({turn: this.turns.findIndex(t => t.id === currentId)});
    }

    // Create multiple chat messages
    await ChatMessage.implementation.create(messages);
    return this;
  }

  async nextRound() {
    await game.combat.resetAll();

    await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.n.newTurn'),
        content: game.i18n.localize('earthdawn.i.initiativeReminder'),
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: () => {
              resolve({});
            },
          },
        },
        default: game.i18n.localize('earthdawn.o.ok'),
      }).render(true);
    });

    //const combatants = this.data.combatants;
    // V10 changes
    const combatants = this.combatants;
		// change End
    for (const element of combatants) {
      await game.combat.rollAll();
      await game.combat.setupTurns();
      console.log('New Round Has Started');
      return super.nextRound();
    }
  }
}
