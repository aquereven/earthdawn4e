import { chatOutput } from '../helpers/chat-message.js';
//import { optionBox } from '../helpers/option-box.js';
import { getOptionBox } from '../helpers/option-box.js';
import { karmaAllowed } from '../helpers/karma-allowed.js';
import { devotionAllowed } from '../helpers/devotion-allowed.js';
import { get1eDice } from '../helpers/get-1e-dice.js';
import { get3eDice } from '../helpers/get-3e-Dice.js';
import { getCeDice } from '../helpers/get-ce-dice.js';
import {getDice} from '../helpers/get-dice.js';
import { actionTestModifiers, defenseModifiers, damageTestModifiers, initiativeMod } from '../helpers/combat-modifiers.js';
import { rollPrep } from '../helpers/roll-prep.js';
import {explodify} from '../helpers/explodify.js'

export default class earthdawn4eActor extends Actor {
  /**
   * @override
   * We override this with an empty implementation because we have our own custom way of applying
   * {@link ActiveEffect}s and {@link Actor#prepareEmbeddedDocuments} calls this.
   */
  applyActiveEffects() {
    return;
  }

  prepareData() {
    //this.data.reset();
    // V10 changes
    //TODO - this.reset(); verursacht einen Fehler, verstehe ich nicht
    //this.reset();
    // change End

    this.prepareBaseData();
    const baseCharacteristics = [

      'system.attributes.dexterityvalue',
      'system.attributes.strengthvalue',
      'system.attributes.toughnessvalue',
      'system.attributes.perceptionvalue',
      'system.attributes.willpowervalue',
      'system.attributes.charismavalue',
      'system.wounds',
      'system.karma.value',
      'system.bonuses.allRollsStep'
    ]

    const actorData = this;
    if (actorData.type === 'pc') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNPCData(actorData);
    if (actorData.type === 'creature') this._preparecreatureData(actorData);

    this.overrides = {};
    this._applyBaseEffects(baseCharacteristics);

    if (actorData.type === 'pc') {
      this.prepareEmbeddedDocuments();
      this._prepareDerivedData(actorData);
      this._applyDerivedEffects(baseCharacteristics);
      this._diceConvert(actorData);
    }
  }

  _prepareCharacterData(actorData) {
  
    const systemData = actorData.system;
    systemData.spells = this.getSpells();
    systemData.movementFinal = Number(systemData.movement) + Number(systemData.overrides.movement);
    systemData.bloodMagicDamage = this.bloodMagicCount() + systemData.overrides.bloodMagicDamage;
    systemData.bloodMagicWounds = 0 + systemData.overrides.bloodMagicWounds;
    systemData.woundsCurrent = systemData.wounds + systemData.bloodMagicWounds;

    systemData.dailytests = Math.ceil(systemData.attributes.toughnessvalue / 6);
    systemData.karma.max = this.karmaCalc() + systemData.unspentattributepoints;
    systemData.devotion.max = this.devotionCalc();
    systemData.bonuses = {};
    // change End

    /** These bonuses stats are to allow attack actions (which typically aren't part of the Actor) to receive bonuses from Active Effects.
     * They are added to the attack/damage as part of the prep functions
     * Note that, currently, Attack Items aren't considered either Melee or Ranged attacks, and thus won't get bonuses from these
     */
    systemData.bonuses.closeAttack = 0;
    systemData.bonuses.closeDamageStep = 0;
    systemData.bonuses.rangedAttack = 0;
    systemData.bonuses.rangedDamageStep = 0;
    systemData.bonuses.allAttack = 0;
    systemData.bonuses.allDamageStep = 0;
    systemData.bonuses.allRollsStep = 0;

  }

  _applyBaseEffects(baseCharacteristics) {
    let overrides = {};

    // Organize non-disabled effects by their application priority
    //baseCharacteristics is list of attributes that need to have Effects applied before Derived Characteristics are calculated

    const changes = this.effects.reduce((changes, e) => {
     

      //if (e.data.changes.length < 1) {
      // V10 changes
      if (e.changes.length < 1) {
      // change End
       
        return changes;
      }

      //if (e.data.disabled || e.isSuppressed || !baseCharacteristics.includes(e.data.changes[0].key)) {
      // V10 changes
      if (e.disabled || e.isSuppressed || !baseCharacteristics.includes(e.changes[0].key)) {
      // change End
        return changes;
      }

      return changes.concat(
        //e.data.changes.map((c) => {
        // V10 changes
        e.changes.map((c) => {
        // change End
          c = foundry.utils.duplicate(c);
          c.effect = e;
          c.priority = c.priority ?? c.mode * 10;
          return c;
        }),
      );
    }, []);

    changes.sort((a, b) => a.priority - b.priority);

    // Apply all changes
    for (let change of changes) {
      const result = change.effect.apply(this, change);
      if (result !== null) overrides[change.key] = result[change.key];
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject({ ...foundry.utils.flattenObject(this.overrides), ...overrides });
  }

  _prepareDerivedData(actorData) {
    if (actorData.type === 'pc') {
    
      const systemData = actorData.system;
            this.threadBonusCount();

            let durabilityData = this.getDurability();
            
            systemData.dexterityStep = this.getStep(systemData.attributes.dexterityvalue);
            systemData.strengthStep = this.getStep(systemData.attributes.strengthvalue);
            systemData.toughnessStep = this.getStep(systemData.attributes.toughnessvalue);
            systemData.perceptionStep = this.getStep(systemData.attributes.perceptionvalue);
            systemData.willpowerStep = this.getStep(systemData.attributes.willpowervalue);
            systemData.charismaStep = this.getStep(systemData.attributes.charismavalue);
            systemData.recoverytestsrefresh = Math.ceil(systemData.attributes.toughnessvalue / 6);
            

            systemData.unconsciousthreshold = systemData.attributes.toughnessvalue * 2;
            
            systemData.durability = durabilityData.healthRating;
            systemData.durability2 = systemData.durability;
            systemData.deathThreshold = systemData.unconsciousthreshold + systemData.toughnessStep;
            systemData.unconscious.max = systemData.unconsciousthreshold + systemData.durability + systemData.overrides.unconsciousrating - systemData.bloodMagicDamage;
            systemData.damage.max = systemData.deathThreshold + systemData.durability + durabilityData.highestCircle + systemData.overrides.deathrating - systemData.bloodMagicDamage;

            systemData.unconscious.value = systemData.damage.value;
            systemData.encumbrance = this.carryingCount();

            systemData.recoverytestsrefreshFinal = systemData.recoverytestsrefresh + systemData.overrides.recoverytestsrefresh + this.permanentMod('recovery');
            systemData.initiativeStep = systemData.dexterityStep - this.armorInitiativePenalty() + this.permanentMod('initiative') + initiativeMod(this) + systemData.bonuses.allRollsStep - systemData.wounds;
            systemData.physicaldefense =
              Math.floor((systemData.attributes.dexterityvalue + 3) / 2) +
              systemData.overrides.physicaldefense +
              this.permanentMod('physicaldefense') +
              this.defenseCount('physicaldefense') +
              this.physicalDefensemod();
            systemData.mysticdefense =
              Math.floor((systemData.attributes.perceptionvalue + 3) / 2) +
              systemData.overrides.mysticdefense +
              this.permanentMod('mysticdefense') +
              this.defenseCount('mysticdefense') +
              this.mysticDefensemod();
            systemData.socialdefense =
              Math.floor((systemData.attributes.charismavalue + 3) / 2) + this.permanentMod('socialdefense') + systemData.overrides.socialdefense;
            systemData.woundThreshold = Math.ceil((systemData.attributes.toughnessvalue + 4) / 2) + this.permanentMod('woundthreshold');
            systemData.woundThreshold += systemData.overrides.woundthreshold ? systemData.overrides.woundthreshold : 0;
            systemData.carryingCapacity = this.carryingCapacity() + this.permanentMod('carryingcapacity');
            systemData.physicalarmor = this.armorCount('Aphysicalarmor') + +this.permanentMod('physicalarmor') + systemData.overrides.physicalarmor;
            systemData.mysticarmor =
              this.armorCount('Amysticarmor') +
              this.permanentMod('mysticarmor') +
              Math.floor(systemData.attributes.willpowervalue / 5) +
              systemData.overrides.mysticarmor;
  
    }
  }

  _applyDerivedEffects(baseCharacteristics) {
    const overrides = {};

    // Organize non-disabled effects by their application priority
    const changes = this.effects.reduce((changes, e) => {
      //if (e.data.changes.length < 1) {
      // V10 changes
      if (e.changes.length < 1) {
      // change End
        return changes;
      }

      //if (e.data.disabled || e.isSuppressed || baseCharacteristics.includes(e.data.changes[0].key)) {
      // V10 changes
      if (e.disabled || e.isSuppressed || baseCharacteristics.includes(e.changes[0].key)) {
      // change End
        return changes;
      }

      return changes.concat(
        //e.data.changes.map((c) => {
        // V10 changes
        e.changes.map((c) => {
        // change End
          c = foundry.utils.duplicate(c);
          c.effect = e;
          c.priority = c.priority ?? c.mode * 10;
          return c;
        }),
      );
    }, []);

    changes.sort((a, b) => a.priority - b.priority);

    // Apply all changes
    for (let change of changes) {
      const result = change.effect.apply(this, change);
      if (result !== null) overrides[change.key] = result[change.key];
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject({ ...foundry.utils.flattenObject(this.overrides), ...overrides });
  }

  _diceConvert(actorData) {
 
    const systemData = actorData.system;
    systemData.dexterityDice = getDice(systemData.dexterityStep);
    systemData.strengthDice = getDice(systemData.strengthStep);
    systemData.toughnessDice = getDice(systemData.toughnessStep);
    systemData.perceptionDice = getDice(systemData.perceptionStep);
    systemData.willpowerDice = getDice(systemData.willpowerStep);
    systemData.charismaDice = getDice(systemData.charismaStep);
    systemData.initiativeDice = getDice(systemData.initiativeStep);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.unconsciousleft.max = systemData.unconscious.max;
    systemData.unconsciousleft.value = systemData.unconscious.max - systemData.damage.value;
  }

  _prepareNPCData(actorData) {
    // const data = actorData.data;
    // data.bonuses = {};

    // V10 changes
    const systemData = actorData.system;
    systemData.bonuses = {};
    // change End

    /** These bonuses stats are to allow attack actions (which typically aren't part of the Actor) to receive bonuses from Active Effects.
     * They are added to the attack/damage as part of the prep functions
     * Not that, currently, Attack Items aren't considered either Melee or Ranged attacks, and thus won't get bonuses from these
     */
    systemData.bonuses.closeAttack = 0;
    systemData.bonuses.closeDamageStep = 0;
    systemData.bonuses.rangedAttack = 0;
    systemData.bonuses.rangedDamageStep = 0;
    systemData.bonuses.allAttack = 0;
    systemData.bonuses.allDamageStep = 0;
    systemData.bonuses.allRollsStep = 0;
    systemData.modifiedInitiative = Number(systemData.initiativeStep) - Number(systemData.wounds) + Number(initiativeMod(this)) + Number(systemData.bonuses.allRollsStep);
    systemData.initiativeDice = getDice(systemData.modifiedInitiative);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.damage.max = systemData.deathThreshold;
    systemData.unconscious.max = systemData.unconsciousThreshold;
    systemData.dexterityDice = getDice(systemData.dexterityStep);
    systemData.strengthDice = getDice(systemData.strengthStep);
    systemData.toughnessDice = getDice(systemData.toughnessStep);
    systemData.perceptionDice = getDice(systemData.perceptionStep);
    systemData.willpowerDice = getDice(systemData.willpowerStep);
    systemData.charismaDice = getDice(systemData.charismaStep);
    systemData.unconsciousleft.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.value = systemData.unconsciousThreshold - systemData.damage.value;
    systemData.woundsCurrent = systemData.wounds;
    // change End
    this.prepareEmbeddedDocuments();
  }

  _preparecreatureData(actorData) {

    const systemData = actorData.system;
    systemData.bonuses = {}
    systemData.bonuses.allRollsStep = 0
    systemData.modifiedInitiative = Number(systemData.initiativeStep) + Number(initiativeMod(this)) + Number(systemData.wounds) + Number(systemData.bonuses.allRollsStep);
    console.log(systemData.modifiedInitiative);
    systemData.initiativeDice = getDice(systemData.modifiedInitiative);
    systemData.initiativeRoll = explodify(systemData.initiativeDice);
    systemData.damage.max = systemData.deathThreshold;
    systemData.unconscious.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.max = systemData.unconsciousThreshold;
    systemData.unconsciousleft.value = systemData.unconsciousThreshold - systemData.damage.value;
  }

  threadBonusCount() {
    // const data = this.data;
    // const talents = data.items.filter(function (item) {
    // V10 changes
    const systemData = this;
    const talents = systemData.items.filter(function (item) {
    // change End
      return item.type === 'talent';
    });
    // const threads = data.items.filter(function (item) {
    //   return item.type === 'thread' && item.data.data.active === true;
    // V10 changes
    const threads = systemData.items.filter(function (item) {
      return item.type === 'thread' && item.system.active === true;
    // change End
    });
    for (const element of talents) {
      let matchingThread = threads.filter(function (thread) {
        //return thread.data.data.characteristic === element.id;
        // V10 changes
        return thread.system.characteristic === element.id;
        // change End
      });
      let talentBonus = 0;
      if (matchingThread.length > 0) {
        //talentBonus = matchingThread[0].data.data.rank;
        // V10 changes
        talentBonus = matchingThread[0].system.rank;
        // change End
      }
      //element.data.data.finalranks = Number(element.data.data.ranks) + Number(talentBonus);
      // V10 changes
      element.system.finalranks = Number(element.system.ranks) + Number(talentBonus);
      // change End
    }
  }

  armorCount(type) {
    // const data = this.data;
    // const armor = data.items.filter(function (item) {
    //   return item.type === 'armor' && item.data.data.worn === true;

    // V10 changes
    const systemData = this;
    const armor = systemData.items.filter(function (item) {
      return item.type === 'armor' && item.system.worn === true;
    // change End
    });

    let runningtotal = 0;

    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    //if (namegiver.length > 0 && game.i18n.localize(namegiver[0].data.name) === 'Obsidiman' && type === 'Aphysicalarmor') {
    // V10 changes
    if (namegiver.length > 0 && game.i18n.localize(namegiver[0].system.name) === 'Obsidiman' && type === 'Aphysicalarmor') {
    // change End

      runningtotal += 3;
    }
    for (const element of armor) {
      if (type === 'Aphysicalarmor') {
        // runningtotal += element.data.data.physicalArmorFinal;
        // V10 changes
        runningtotal += element.system.physicalArmorFinal;
        // change End
      } else if (type === 'Amysticarmor') {
        // runningtotal += element.data.data.mysticArmorFinal;
        // V10 changes
        runningtotal += element.system.mysticArmorFinal;
        // change End
      }
    }

    return runningtotal;
  }

  armorInitiativePenalty() {
    // const data = this.data;
    // const armor = data.items.filter(function (item) {
    //   return item.type === 'armor' && item.data.data.worn === true;
      // V10 changes
      const systemData = this;
      const armor = systemData.items.filter(function (item) {
        return item.type === 'armor' && item.system.worn === true;
      // change End
    });
    // const shield = data.items.filter(function (item) {
    //   return item.type === 'shield' && item.data.data.worn === true;
      // V10 changes
      const shield = systemData.items.filter(function (item) {
        return item.type === 'shield' && item.system.worn === true;
      // change End
    });
    let penalty = 0;
    if (armor.length > 0) {
      // penalty += armor[0].data.data.armorPenalty;
      // V10 changes
      penalty += armor[0].system.armorPenalty;
      // change End
    }
    if (shield.length > 0) {
      // penalty += shield[0].data.data.initiativepenalty;
      // V10 changes
      penalty += shield[0].system.initiativepenalty;
      // change End
    }

    return penalty;
  }

  karmaCalc() {
    // const data = this.data;
    // const namegiver = data.items.filter(function (item) {
    // V10 changes
    const systemData = this;
    const namegiver = systemData.items.filter(function (item) {
    // change End
      return item.type === 'namegiver';
    });
    let karmamod = 0;
    if (namegiver.length > 0) {
      // karmamod = namegiver[0].data.data.karmamodifier;
      // V10 changes
      karmamod = namegiver[0].system.karmamodifier;
      // change End
    }
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    let totalcircles = 0;

    for (const element of disciplines) {
      // let discCircle = Number(element.data.data.circle);
      // V10 changes
      let discCircle = Number(element.system.circle);
      // change End
      if (discCircle > totalcircles) {
        totalcircles = discCircle;
      }
    }

    let circles = totalcircles;
    return Number(circles * karmamod);
  }

  devotionCalc() {
    // const data = this.data;
    // V10 changes
    const data = this.system;
    // change End
    const questor = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    let rank = 0;
    for (let i = 0; i < questor.length; i++) {
      // if (questor[i].data.data.discipline === 'questor') {
      //   let discrank = questor[i].data.data.circle;
      // V10 changes
      if (questor[i].system.discipline === 'questor') {
        let discrank = questor[i].system.circle;
      // change End
        rank = discrank;
      }
    }
    let questorrank = rank;
    return Number(questorrank * 10);
  }

  defenseCount(type) {
    // const data = this.data;
    // V10 changes
    const systemData = this;
    // change End
    // const shield = data.items.filter(function (item) {
    //   return item.type === 'shield' && item.data.data.worn === true;
    // V10 changes
    const shield = systemData.items.filter(function (item) {
      return item.type === 'shield' && item.system.worn === true;
    // change End
    });
    let runningtotal = 0;
    let shieldvalue = 0;

    for (const element of shield) {
      //shieldvalue = Number(element.data.data[type]);
      // V10 changes
      shieldvalue = Number(element.system[type]);
      // change End
      runningtotal += shieldvalue;
    }

    return runningtotal;
  }

  permanentMod(input) {
    // const data = this.data;
    // V10 changes
    const systemData = this;
    // change End
    // const namegiver = data.items.filter(function (item) {
    // V10 changes
    const namegiver = systemData.items.filter(function (item) {
    // change End
      return item.type === 'namegiver';
    });
    let runningtotal = 0;
    for (let i = 0; i < namegiver.length; i++) {
      //if (namegiver[i].data.data.bonus1.characteristic === input) runningtotal += namegiver[i].data.data.bonus1.value;
      // V10 changes
      if (namegiver[i].system.bonus1.characteristic === input) runningtotal += namegiver[i].system.bonus1.value;
      // change End
    }

    // const disciplines = data.items.filter(function (item) {
    //   return item.type === 'discipline' && item.data.data.discipline !== 'path';
    // V10 changes
    const disciplines = systemData.items.filter(function (item) {
      return item.type === 'discipline' && item.system.discipline !== 'path';
    // change End
    });

    // const paths = data.items.filter(function (item) {
    //   return item.type === 'discipline' && item.data.data.discipline === 'path';
    // V10 changes
    const paths = systemData.items.filter(function (item) {
      return item.type === 'discipline' && item.system.discipline === 'path';
    // change End
    });

    let workingdiscipline;
    let disciplineBonus = 0;
    let workingpath;
    let p;
    let modifiedDisciplineName;
    let pathModifier = 0;
    let totalCircles = 0;

    // get any bonus from Path for INPUT
    for (p = 0; p < paths.length; p++) {
      // workingpath = paths[p].data;
      // modifiedDisciplineName = workingpath.data.relatedDiscipline;
      // V10 changes
      workingpath = paths[p];
      modifiedDisciplineName = workingpath.system.relatedDiscipline;
      // change End
      var r;
      // for (r = 1; r <= workingpath.data.circle; r++) {
      //   let characteristic = getProperty(workingpath, `data.circle${r}.characteristic`);
      //   if (characteristic === input && workingpath.data['circle' + r].value > pathModifier) {
      //     pathModifier = workingpath.data['circle' + r].value;
      // V10 changes
      for (r = 1; r <= workingpath.system.circle; r++) {
        let characteristic = getProperty(workingpath, `system.circle${r}.characteristic`);
        if (characteristic === input && workingpath.system['circle' + r].value > pathModifier) {
          pathModifier = workingpath.system['circle' + r].value;
      // change End
        }
      }
    }

    // get any bonus from Discipline for INPUT
    let d;
    for (d = 0; d < disciplines.length; d++) {
      //workingdiscipline = disciplines[d].data;
      // V10 changes
      workingdiscipline = disciplines[d];
      // change End
      let modifiedValue;
      var r;
      //for (r = 1; r <= workingdiscipline.data.circle; r++) {
        //let characteristic = getProperty(workingdiscipline, `data.circle${r}.characteristic`);
        // V10 changes
        for (r = 1; r <= workingdiscipline.system.circle; r++) {
        let characteristic = getProperty(workingdiscipline, `system.circle${r}.characteristic`);
        // change End

        if (characteristic === input) {
          if (workingdiscipline.name === modifiedDisciplineName) {
            // modifiedValue = workingdiscipline.data['circle' + r].value + pathModifier;
            // V10 changes
            modifiedValue = workingdiscipline.system['circle' + r].value + pathModifier;
            // change End
          } else {
            //modifiedValue = workingdiscipline.data['circle' + r].value;
            // V10 changes
            modifiedValue = workingdiscipline.system['circle' + r].value;
            // change End
          }
          if (characteristic === input && modifiedValue > disciplineBonus) {
            disciplineBonus = modifiedValue;
          }
        }
      }
    }

    runningtotal += disciplineBonus;

    // Get any bonus from Thread Items for INPUT
    // const threadItem = data.items.filter(function (item) {
    // V10 changes
    const threadItem = systemData.items.filter(function (item) {
    // change End
      return (
        (item.type === 'weapon' || item.type === 'shield' || item.type === 'armor' || item.type === 'equipment') &&
        // item.data.data.numberthreads > 0
        // V10 changes
        item.system.numberthreads > 0
        // change End
      );
    });

    for (let i = 0; i < threadItem.length; i++) {
      // let ranks = threadItem[i].data.data.numberthreads;
      // V10 changes
      let ranks = threadItem[i].system.numberthreads;
      // change End
      let r;
      for (r = 1; r <= ranks; r++) {
        let rankKey = `rank${r}`;
        // let threadRank = threadItem[i].data.data.threads[rankKey];
        // V10 changes
        let threadRank = threadItem[i].system.threads[rankKey];
        // change End

        if (threadRank.threadactive === true && threadRank.characteristic === input) {
          runningtotal += Number(threadRank.value);
        }
      }
    }

    // Get any bonus from Threads for INPUT
    // const threads = data.items.filter(function (item) {
    //   return item.type === 'thread' && item.data.data.active === true;
    // V10 changes
    const threads = systemData.items.filter(function (item) {
      return item.type === 'thread' && item.system.active === true;
    // change End
    });

    for (let i = 0; i < threads.length; i++) {
      // if (threads[i].data.data.characteristic === input) runningtotal += Number(threads[i].data.data.rank);
      // V10 changes
      if (threads[i].system.characteristic === input) runningtotal += Number(threads[i].system.rank);
      // change End
    }

    return runningtotal;
  }

  physicalDefensemod() {
    let mod = defenseModifiers(this);
    return mod;
  }

  mysticDefensemod() {
    let mod = defenseModifiers(this);
    return mod;
  }

  carryingCount() {
    // const data = this.data;
    // const stuff = data.items.filter(function (item) {
    // V10 changes
    const systemData = this;
    const stuff = systemData.items.filter(function (item) {
    // change End
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      // if (stuff[i].data.data.weight > 0) {
      //   runningtotal += Number(stuff[i].data.data.weight);
      // V10 changes
      if (stuff[i].system.weight > 0) {
        runningtotal += Number(stuff[i].system.weight);
      // change End
      }
    }
    return runningtotal;
  }

  bloodMagicCount() {
    // const data = this.data;
    // const stuff = data.items.filter(function (item) {
    // V10 changes
    const systemData = this;
    const stuff = systemData.items.filter(function (item) {
    // change End
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      // if (stuff[i].data.data.bloodMagicDamage > 0) {
      //   runningtotal += Number(stuff[i].data.data.bloodMagicDamage);
      // V10 changes
      if (stuff[i].system.bloodMagicDamage > 0) {
        runningtotal += Number(stuff[i].system.bloodMagicDamage);
      // change End
      }
    }
    return runningtotal;
  }

  carryingCapacity() {
    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    // let s = this.data.data.attributes.strengthvalue;
    // if (namegiver.length > 0 && game.i18n.localize(namegiver[0].data.name) === 'Dwarf') {
    // V10 changes
    let s = this.system.attributes.strengthvalue;
    if (namegiver.length > 0 && game.i18n.localize(namegiver[0].system.name) === 'Dwarf') {
    // change End
      s += 2;
    }
    let t = Math.ceil(s / 5);
    return -12.5 * t ** 2 + 5 * t * s + 12.5 * t + 5;
  }

  getDice(step) {
    let dice = 0;
    if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step4') {
      var stepTable = [
        '0',
        '1d4-2',
        '1d4-1',
        '1d4',
        '1d6',
        '1d8',
        '1d10',
        '1d12',
        '2d6',
        '1d8+1d6',
        '2d8',
        '1d10+1d8',
        '2d10',
        '1d12+1d10',
        '2d12',
        '1d12+2d6',
        '1d12+1d8+1d6',
        '1d12+2d8',
        '1d12+1d10+1d8',
        '1d20+2d6',
        '1d20+1d8+1d6',
      ];
      if (step < 0) {
        return;
      } else if (step < 19) {
        dice = stepTable[step];
      } else {
        let i = step;
        let loops = 0;
        while (i > 18) {
          loops += 1;
          i -= 11;
        }
        dice = loops + 'd20+' + stepTable[i];
      }
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step1') {
      dice = get1eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step3') {
      dice = get3eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'stepC') {
      dice = getCeDice(step);
    }
    return dice;
  }

  getStep(value) {
    if (!value > 0) {
      return 0;
    } else {
      return Number([Math.ceil(value / 3) + 1]);
    }
  }

  async targetDifficulty() {
    const targets = Array.from(game.user.targets);
    var physicaldefense = 0;
    if (targets.length > 0) {
      // physicaldefense = targets[0].actor.data.data.physicaldefense;
      // V10 changes
      physicaldefense = targets[0].actor.system.physicaldefense;
      // change End
      if (targets[0].actor.type !== 'pc') {
        physicaldefense += defenseModifiers(targets[0].actor);
      }
    }
    return physicaldefense;
  }

    getDurability() {
    // const disciplines = this.items.filter(function (item) {
    //   return item.type === 'discipline';
    // });

    // V10 changes
    const systemData = this;

    const disciplines = systemData.items.filter(function (item) {

      return item.type === 'discipline';
    });
    // change End


    let runningtotal = 0;
    var discCircle = 0;
    var discDura = 0;

    // ERRATTA https://docs.google.com/document/d/1ts_bx53HtB5-ThUxzHFsCPM4-g9wjIzHdJLdGy4XcfY/edit#heading=h.bx1zxh37l1bb
    // Durability Improvement with Thread Magic, Page 230
    // Clarification:
    // When Durability is improved through the use of thread magic(e.g.pattern items or thread items),
    // the adept’s highest Circle is effectively increased by the bonus for determining their Durability.
    // For example, Beispeil has a Band of the Elements, which gives them + 1 Durability.
    // Their highest Circle is in Example (Circle 7).
    // This improves them to Circle 8 for the purposes of determining their Durability.

    let duraCircleMod = Number(this.permanentMod('durability'));

    // find the highest circle
    let highest = { id: '', circle: 0, durability: 0 };
    for (const element of disciplines) {
      // let circle = Number(element.data.data.circle);
      // let durability = Number(element.data.data.durability);
      // V10 changes
      let circle = Number(element.system.circle);
      let durability = Number(element.system.durability);
      // change End

      // save some data as we try to determine the highest circle
      // highest circle is disc with the highest circle and the largest durability
      // in case of a tie
      if (circle > highest.circle) {
        highest.id = element._id;
        highest.circle = circle;
        highest.durability = durability;
      } else if (circle === highest.circle && durability > highest.durability) {
        highest.id = element._id;
        highest.durability = durability;
      }
    }

    // operate on the highest durablity first as it overrides a lower durability
    //disciplines.sort((a, b) => (a.data.data.durability > b.data.data.durability ? -1 : 1));
    // V10 changes
    disciplines.sort((a, b) => (a.system.durability > b.system.durability ? -1 : 1));
    // change End

    for (const element of disciplines) {
      // let circle = Number(element.data.data.circle);
      // V10 changes
      let circle = Number(element.system.circle);
      // change End

      // if this discipline matches the one tagged as the highest circle discipline, apply the duraCircleMod
      circle = element._id === highest.id ? circle + duraCircleMod : circle;

      if (circle - discCircle > 0) {
        discCircle = circle - discCircle;
        // discDura = Number(element.data.data.durability);
        // V10 changes
        discDura = Number(element.system.durability);
        // change End
      } else {
        discDura = 0;
      }

      let total = discDura * discCircle;
      runningtotal += total;
    }
    return { healthRating: runningtotal, highestCircle: highest.circle };
  }

  getSpells() {
    const spells = this.items.filter(function (item) {
      return item.type === 'spell';
    });
  }

  getAttack(weapontype) {
    let attacktype = weapontype;
    let attackname = '';
    if (attacktype === 'Melee') {
      attackname = game.i18n.localize('earthdawn.m.melee_weapon');
    } else if (attacktype === 'Ranged') {
      attackname = game.i18n.localize('earthdawn.m.missile_weapon');
    } else if (attacktype === 'Thrown') {
      attackname = game.i18n.localize('earthdawn.t.thrown_weapon');
    } else if (attacktype === 'Unarmed') {
      attackname = game.i18n.localize('earthdawn.u.unarmed_combat');
    }

    let attack = this.items.filter(function (item) {
      return item.name === `${attackname}` && item.type === 'talent';
    });
    if (attack.length < 1) {
      attack = this.items.filter(function (item) {
        return item.name === `${attackname}` && item.type === 'skill';
      });
    }
    if (attack.length < 1) {
      return;
    } else {
      return attack[0].id;
    }
  }

  async halfMagic() {
    const inputs = {
      //wounds: this.data.data.woundsCurrent,
      // V10 changes
      wounds: this.system.woundsCurrent,
    // change End
    };
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    var disciplinelist = `
    <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.discipline')}</label>
    <select class='misc-mod-input' id='discipline_box'>
    `;
    var i;
    var disciplineId;
    var disciplineName;
    for (i = 0; i < disciplines.length; i++) {
      // disciplineId = disciplines[i].data._id;
      // disciplineName = disciplines[i].data.name;
      // V10 changes
      disciplineId = disciplines[i]._id;
      disciplineName = disciplines[i].name;
      // change End
      disciplinelist += `<option value='${disciplineId}'>${disciplineName}</option>`;
    }
    disciplinelist += '</select>';

    const html = `
        <div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.a.attribute')}: </label>
                <select id='attribute_box'>
                    <option value='dexterityStep'>Dexterity</option>
                    <option value='strengthStep'>Strength</option>
                    <option value='toughnessStep'>Toughness</option>
                    <option value='perceptionStep'>Perception</option>
                    <option value='willpowerStep'>Willpower</option>
                    <option value='charismaStep'>Charisma</option>
                </select>
            </div>
            <div class='misc-mod'>${disciplinelist}</div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input class='misc-mod-input circleInput' id='dialog_box' value='0' autofocus/>
            </div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.difficulty')}: </label>
                <input class='misc-mod-input circleInput' id='difficulty_box' value='0' data-type='number' />
            </div>
        </div>
        <div >
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input class='misc-mod-input circleInput' id='karma_box' value='0' data-type='number' />
            </div>
        </div>
        <div >
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.d.devotion')}: </label>
                <input class='misc-mod-input circleInput' id='devotion_box' value='0' data-type='number' />
            </div>
        </div>
        <div>
            <select name='system.devotionDie' id='devotionDie_box' value='${inputs.devotionDie}' {{#select system.devotionDie}}>
                <option value='na'>na</option>
                <option value='d4'>${game.i18n.localize('earthdawn.d.d')}4</option>
                <option value='d6'>${game.i18n.localize('earthdawn.d.d')}6</option>
                <option value='d8'>${game.i18n.localize('earthdawn.d.d')}8</option>
                {{/select}}
            </select>
        </div>
        `;
        // V10 changes --- changed the following line inside the above html
        // <select name='data.devotionDie' id='devotionDie_box' value='${inputs.devotionDie}' {{#select data.data.devotionDie}}>
        // change End
    let miscmod = await new Promise((resolve) => {
      const sheet = this._sheet;
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                attribute: html.find('#attribute_box').val(),
                discipline: html.find('#discipline_box').val(),
                modifier: html.find('#dialog_box').val(),
                difficulty: html.find('#difficulty_box').val(),
                karma: html.find('#karma_box').val(),
                devotion: html.find('#devotion_box').val(),
                devotionDie: html.find('#devotionDie_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let activediscipline = this.items.get(`${miscmod.discipline}`);
    // let circle = Number(activediscipline.data.data.circle);
    // let attributestep = Number(this.data.data[miscmod.attribute]);

    // V10 changes
    let circle = Number(activediscipline.system.circle);
    let attributestep = Number(this.system[miscmod.attribute]);
    // change End

    let basestep = Number(circle + attributestep + Number(miscmod.modifier));

    //inputs.talent = `${game.i18n.localize('earthdawn.h.halfmagic')} (${activediscipline.data.name})`;
    // V10 changes
    inputs.talent = `${game.i18n.localize('earthdawn.h.halfmagic')} (${activediscipline.name})`;
    // change End
    inputs.karma = miscmod.karma;
    inputs.devotion = miscmod.devotion;
    inputs.devotionDie = miscmod.devotionDie;

    inputs.finalstep = basestep + Number(miscmod.modifier);
    // if (this.data.data.tactics.defensive || this.data.data.tactics.knockeddown === true) {
    // V10 changes
    if (this.system.tactics.defensive || this.system.tactics.knockeddown === true) {
    // change End
      inputs.finalstep -= 3;
    }
    this.rollDice(inputs);
  }

  async knockdownTest(inputs) {
    let result = {};
    // if (this.data.data.tactics.knockeddown === true){
    // V10 changes
    if (this.system.tactics.knockeddown === true){
    // change End
      ui.notifications.info(`Character is already Knocked Down`)
      return
    }
    if (this.type !== 'pc') {
      inputs.difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs.strain = inputs.strain ? inputs.strain : 0;
      inputs.karma = inputs.karma ? inputs.karma : 0;
      inputs.modifier = inputs.modifier ? inputs.modifier : 0;
      inputs.devotion = inputs.devotion ? inputs.devotion : 0;
      //let miscmod = await optionBox(inputs, []);
      let miscmod = await getOptionBox(inputs);
      if (miscmod.cancel) {
        miscmod={}
      }
      inputs = {
        //finalstep: this.data.data.knockdown,
        // V10 changes
        finalstep: this.system.knockdown,
        // change End
        rolltype: 'knockdown',
        talent: 'Knockdown',
        difficulty: miscmod.difficulty,
      };
      result = await this.rollDice(inputs);
    } else {
      let difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs = {
        attribute: 'strengthStep',
        rolltype: 'knockdown',
        name: 'Knockdown',
        difficulty: difficulty,
        title: game.i18n.localize('earthdawn.k.knockdownTest'),
      };
      result = await rollPrep(this, inputs);
    }
    console.log('[EARTHDAWN] Knockdown Test', inputs, result);
    if (result.margin < 0) {
      // this.update({ 'data.tactics.knockeddown': true });
      // V10 changes
      this.update({ 'system.tactics.knockeddown': true });
      // change End
    }
  }

  async jumpUpTest(inputs) {
    inputs = { attribute: 'dexterityStep', name: 'Jump Up', difficulty: 6, strain: 2, rolltype: 'jumpUp' };
    console.log(this)
    let results = await rollPrep(this, inputs);
    console.log(this)
    if (results.margin >= 0) {
      // this.update({ 'data.tactics.knockeddown': false });
      // V10 changes
      this.update({ 'system.tactics.knockeddown': false });
      // change End
    } else if (results.margin < 0) {
      // this.update({ 'data.tactics.knockeddown': true });
      // V10 changes
      this.update({ 'system.tactics.knockeddown': true });
      // change End
    }
  }

  async attuneMatrix(matrix) {
    let spellList = this.items.filter(function (item) {
      // return item.type === 'spell' && item.data.data.circle <= matrix.data.data.circle;
      // V10 changes
      return item.type === 'spell' && item.system.circle <= matrix.system.circle;
      // change End
    });
    let spellOptions = '';
    var arrayLength = spellList.length;
    for (var i = 0; i < arrayLength; i++) {
      let id = spellList[i].id;
      let name = spellList[i].name;
      spellOptions += `<option value='${id}'>${name}</option>`;
    }

    let html =
      '<select id= "spell-name" name="newspell">' +
      spellOptions +
      '</select><div><input type="checkbox" id="attune_fly">' +
      game.i18n.localize('earthdawn.a.attuneOnFly') +
      '</div>';

    let newspell = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.matrixAttune'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                id: html.find('#spell-name').val(),
                onfly: html.find('#attune_fly:checked'),
              });
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
            callback: () => {},
          },
        },
        default: 'ok',
      }).render(true);
    });
    let spellarray = this.items.filter(function (item) {
      return item.id === `${newspell.id}`;
    });
    let spellinfo = spellarray[0];
    // let discipline = this.getWeaving(spellinfo.data.data.discipline);
    // V10 changes
    let discipline = this.getWeaving(spellinfo.system.discipline);
    // change End
    discipline = game.i18n.localize(discipline);
    let itemarray = this.items.filter(function (item) {
      return item.name.includes(`${discipline}`) && item.type === 'talent';
    });
    if (itemarray.length < 1) {
      ui.notifications.error(
        `${game.i18n.localize('earthdawn.y.youDoNotHave')} ${discipline} - ${game.i18n.localize('earthdawn.a.attuneNot')}`,
      );
      return false;
    }
    let itemID = itemarray[0].id;
    if (newspell.onfly.length > 0) {
      let inputs = {
        // difficulty: spellinfo.data.data.reattunedifficulty,
        // V10 changes
        difficulty: spellinfo.system.reattunedifficulty,
        // change End
        talentID: itemID,
      };
      let strainInputs = { damage: 1, ignorearmor: true, type: 'physical' };
      await this.takeDamage(strainInputs);
      let returnvalue = await rollPrep(this, inputs);
      if (returnvalue.margin > 0) {
        matrix.update({
          'system.threadsrequired': spellinfo.system.threadsrequired,
          'system.targetadditionalthreads': 0,
          'system.spellId': spellinfo._id,
          'system.range': spellinfo.system.range,
          'system.currentspell': spellinfo.name,
          'system.weavingdifficulty': spellinfo.system.weavingdifficulty,
          'system.castingdifficulty': spellinfo.system.castingdifficulty,
          'system.activethreads': 0,
          'system.discipline': spellinfo.system.discipline,
          'system.extrathreads': spellinfo.system.extrathreads,
          'system.extrasuccesses': spellinfo.system.extrasuccesses,
          // change End
        });
      } else {
        ui.notifications.info(`${game.i18n.localize('earthdawn.a.attuneFailed')}`);
      }
    } else {
      matrix.update({
        'system.threadsrequired': spellinfo.system.threadsrequired,
        'system.targetadditionalthreads': 0,
        'system.spellId': spellinfo._id,
        'system.range': spellinfo.system.range,
        'system.currentspell': spellinfo.name,
        'system.weavingdifficulty': spellinfo.system.weavingdifficulty,
        'system.castingdifficulty': spellinfo.system.castingdifficulty,
        'system.activethreads': 0,
        'system.discipline': spellinfo.system.discipline,
        'system.extrathreads': spellinfo.system.extrathreads,
        'system.extrasuccesses': spellinfo.system.extrasuccesses,
        // change End
      });
    }
  }

  async weaveThread(matrix) {
    // let thread_weaving = this.getWeaving(matrix.data.data.discipline);
    // let localizedDiscipline = 'earthdawn.d.discipline' + matrix.data.data.discipline;
    // V10 changes
    let thread_weaving = this.getWeaving(matrix.system.discipline);
    let localizedDiscipline = 'earthdawn.d.discipline' + matrix.system.discipline;
    // change End
    let discipline_fallback = getProperty(game.i18n._fallback, `${localizedDiscipline}`);
    localizedDiscipline = game.i18n.localize(localizedDiscipline);
    discipline_fallback = discipline_fallback === null ? game.i18n.localize(localizedDiscipline) : discipline_fallback;
    let thread_weaving_fallback = getProperty(game.i18n._fallback, `${thread_weaving}`);
    thread_weaving_fallback = thread_weaving_fallback === null ? game.i18n.localize(thread_weaving) : thread_weaving_fallback;
    thread_weaving = game.i18n.localize(thread_weaving);
    let disciplineinfo = this.items.filter(function (item) {
      return item.type === `discipline` && (item.name.includes(`${localizedDiscipline}`) || item.name.includes(`${discipline_fallback}`));
    });
    let talent = this.items.filter(function (item) {
      return item.type === `talent` && (item.name.includes(`${thread_weaving}`) || item.name.includes(`${thread_weaving_fallback}`));
    });
    if (talent.length < 1) {
      ui.notifications.info(`${game.i18n.localize('earthdawn.y.youDoNotHave')} ` + thread_weaving + ' or ' + thread_weaving_fallback);
      return false;
    }

    let maxextra = 0;
    if (disciplineinfo.length > 0) {
      let label = game.i18n.localize('earthdawn.e.extraThreads');
      let label2 = game.i18n.localize('earthdawn.e.extraThreadEffects');
      let label3 = game.i18n.localize('earthdawn.e.extraThreadRemember');
      let label4 = game.i18n.localize('earthdawn.e.extra');
      // if (matrix.data.data.targetadditionalthreads === 0 || matrix.data.data.targetadditionalthreads === null) {
      // V10 changes
      if (matrix.system.targetadditionalthreads === 0 || matrix.system.targetadditionalthreads === null) {
      // change End
        if (disciplineinfo.length > 0) {
          // maxextra = Math.ceil(disciplineinfo[0].data.data.circle / 4);
          // V10 changes
          maxextra = Math.ceil(disciplineinfo[0].system.circle / 4);
          // change End
        }
        // let extraThreads = matrix.data.data.extrathreads;
        // V10 changes
        let extraThreads = matrix.system.extrathreads;
        // change End
        extraThreads = extraThreads.replace(/,/g, '</li><li>');
        extraThreads = '<ul><li>' + extraThreads + '</li></ul>';

        let html = `<div class='weave-thread-box'>
                      <label>${label}</label>
                      <input class='circleInput' type='text' id='extra-threads' name='extra-threads' data-dtype='Number' />

                      <br/>${label3} ${maxextra} ${label4}<br/>
                      <label>${label2}:</label>
                      ${extraThreads}
                    </div>`;

        let threadextra = await new Promise((resolve) => {
          new Dialog({
            title: game.i18n.localize('earthdawn.m.matrixWeave'),
            content: html,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.o.ok'),
                callback: (html) => {
                  resolve({
                    extrathreads: html.find('#extra-threads').val(),
                  });
                },
              },
              cancel: {
                label: game.i18n.localize('earthdawn.c.cancel'),
                callback: () => {},
              },
            },
            default: 'ok',
          }).render(true);
        });
        threadextra.extrathreads = threadextra.extrathreads > maxextra ? maxextra : threadextra.extrathreads;
        //await matrix.update({ 'data.targetadditionalthreads': Number(threadextra.extrathreads) });
        // V10 changes
        await matrix.update({ 'system.targetadditionalthreads': Number(threadextra.extrathreads) });
        // change End
      }
    }
    // let parameters = { talentID: talent[0].id, type: 'talent', difficulty: matrix.data.data.weavingdifficulty };
    // V10 changes
    let parameters = { talentID: talent[0].id, type: 'talent', difficulty: matrix.system.weavingdifficulty };
    // change End
    let results = await rollPrep(this, parameters);
    if (Number(results.margin) >= 0) {
      let targetthreads = Number(matrix.system.threadsrequired) + Number(matrix.system.targetadditionalthreads);
      if (matrix.system.activethreads < targetthreads) {
        let newthreads = Number(matrix.system.activethreads) + 1 + Number(results.extraSuccess);
        newthreads = newthreads > targetthreads ? targetthreads : newthreads;
        await matrix.update({ 'system.activethreads': Number(newthreads) });
      // change End
      }
    }
  }

  async castSpell(matrix) {
  
    if (matrix.data.data.totalthreadsneeded > matrix.data.data.totalthreads) {
      // V10 changes

// change End
      ui.notifications.info('Not Enough Threads. Keep Weaving!');
      return false;
    } else {
      let talent = this.items.filter(function (item) {
        return item.type === `talent` && (item.name === `Spellcasting` || item.name === `Spruchzauberei`);
      });
      if (talent.length < 1) {
        ui.notifications.info('Character Does Not Have Spellcasting');
        return;
      }
      var difficulty;
      const targets = Array.from(game.user.targets);
      if (
      //   (matrix.data.data.castingdifficulty.includes('TMD') ||
      //     matrix.data.data.castingdifficulty.includes('MVZ') ||
      //     matrix.data.data.castingdifficulty.includes('MWST') ||
      //     matrix.data.data.castingdifficulty.includes('MVK')) &&
      //   matrix.data.data.range === 'Self'
      // ) {
      //   difficulty = this.data.data.mysticdefense;
      // V10 changes
        (matrix.system.castingdifficulty.includes('TMD') ||
          matrix.system.castingdifficulty.includes('MVZ') ||
          matrix.system.castingdifficulty.includes('MWST') ||
          matrix.system.castingdifficulty.includes('MVK')) &&
        matrix.system.range === 'Self'
      ) {
        difficulty = this.system.mysticdefense;
      // change End
        

      } else if (
        // (matrix.data.data.castingdifficulty === 'TMD' ||
        //   matrix.data.data.castingdifficulty === 'MVZ' ||
        //   matrix.data.data.castingdifficulty === 'MWST' ||
        //   matrix.data.data.castingdifficulty === 'MVK') &&
          // V10 changes
          (matrix.system.castingdifficulty === 'TMD' ||
          matrix.system.castingdifficulty === 'MVZ' ||
          matrix.system.castingdifficulty === 'MWST' ||
          matrix.system.castingdifficulty === 'MVK') &&
          // change End
        targets.length > 0
      ) {
        //difficulty = targets[0].actor.data.data.mysticdefense;
        // V10 changes
        difficulty = targets[0].actor.system.mysticdefense;
        // change End
        if (targets[0].actor.type !== 'pc'){
        difficulty += defenseModifiers(targets[0].actor);}
      // } else if (!isNaN(matrix.data.data.castingdifficulty)) {
      //   difficulty = matrix.data.data.castingdifficulty;
        // V10 changes
      } else if (!isNaN(matrix.system.castingdifficulty)) {
        difficulty = matrix.system.castingdifficulty;
      // change End
      } else {
        difficulty = 0;
      }

    
      let inputs = {
        // spellName: matrix.data.data.currentspell,
        // V10 changes
        spellName: matrix.system.currentspell,
        // change End
        talentID: talent[0].id,
        rolltype: 'spell',
        type: talent[0].type,
        difficulty: difficulty,
        spellId: matrix.system.spellId,
        targets: targets
      };
      console.log(inputs)
      let results = await rollPrep(this, inputs);
      if (results) {
        // await matrix.update({ 'data.activethreads': 0, 'data.targetadditionalthreads': 0 });
        // V10 changes
        await matrix.update({ 'system.activethreads': 0, 'system.targetadditionalthreads': 0 });
        // change End
      }
    }
  }

  rollPrep(inputs){
    //this function exists for backward compatability, the real action now happens in /helpers/roll-prep.js
    rollPrep(this, inputs)
  }

  getWeaving(discipline) {
    let weavingTalent = '';

    switch (discipline) {
      case 'Elementalist':
        weavingTalent = 'earthdawn.e.elementalism';
        return weavingTalent;
      case 'Illusionist':
        weavingTalent = 'earthdawn.i.illusionism';
        return weavingTalent;
      case 'Nethermancer':
        weavingTalent = 'earthdawn.n.nethermancy';
        return weavingTalent;
      case 'Wizard':
        weavingTalent = 'earthdawn.w.wizardry';
        return weavingTalent;
      case 'Shaman':
        weavingTalent = 'earthdawn.s.shamanism';
        return weavingTalent;
    }
  }

  clearMatrix(matrix) {
    matrix.update({
      'system.currentspell': '',
      'system.weavingdifficulty': '',
      'system.targetadditionalthreads': '',
      'system.castingdifficulty': '',
      'system.activethreads': '',
      'system.threadsrequired': '',
    });
  }

  async weaponDamagePrep(weapon, extraSuccess, damageBonus) {
    let damage = '';
    let step = '';
    let damageStep = 0;
    let damageBonus2 = damageBonus;

    if (weapon.type === 'weapon') {
      //damage = weapon.data.data.damageattribute;
      // V10 changes
      damage = weapon.system.damageattribute;
      // change End
      step = damage + 'Step';
    } else if (weapon.type === 'attack') {
      // damageStep = weapon.data.data.damagestep;
      // V10 changes
      damageStep = weapon.system.damagestep;
		  // change End
    } else if (weapon.type === 'talent') {
      //damage = weapon.data.data.attribute;
      // V10 changes
      damage = weapon.system.attribute;
		  // change End
      step = damage;
      //damageStep = weapon.data.data.final;
      // V10 changes
      damageStep = weapon.system.final;
		  // change End
    }

    // let att = this.data.data[step] ? Number(this.data.data[step]) : 0;
    // V10 changes
    let att = this.system[step] ? Number(this.system[step]) : 0;
		// change End
    if (weapon.type === 'talent') {
      // damageStep = att + Number(weapon.data.data.finalranks);
      // V10 changes
      damageStep = att + Number(weapon.system.finalranks);
		  // change End
      if (weapon.name === game.i18n.localize('Claw Shape')) {
        damageStep += 3;
      }
    } else if (weapon.type === 'weapon') {
      // if (weapon.data.data.weapontype === 'Melee' || weapon.data.data.weapontype === 'Unarmed') {
      // V10 changes
      if (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed') {
      // change End
        damageStep =
          att +
      //     Number(weapon.data.data.damageFinal) +
      //     Number(this.data.data.bonuses.closeDamageStep) +
      //     Number(this.data.data.bonuses.allDamageStep);
      // } else if (weapon.data.data.weapontype === 'Ranged' || weapon.data.data.weapontype === 'Thrown') {
          // V10 changes
          Number(weapon.system.damageFinal) +
          Number(this.system.bonuses.closeDamageStep) +
          Number(this.system.bonuses.allDamageStep);
      } else if (weapon.system.weapontype === 'Ranged' || weapon.system.weapontype === 'Thrown') {
      // change End
        damageStep =
          att +
          // Number(weapon.data.data.damageFinal) +
          // Number(this.data.data.bonuses.rangedDamageStep) +
          // Number(this.data.data.bonuses.allDamageStep);
          // V10 changes
          Number(weapon.system.damageFinal) +
          Number(this.system.bonuses.rangedDamageStep) +
          Number(this.system.bonuses.allDamageStep);
		      // change End
      }
    }
    let name = weapon.name;
    // if (this.data.data.tactics.knockeddown === true)
    // V10 changes
    if (this.system.tactics.knockeddown === true)
		// change End
    {damageStep = (damageStep -3);
      if (damageStep < 1){damageStep = 1}}
    let inputs = {
      rolltype: 'weapondamage',
      damagestep: damageStep,
      extraSuccess: extraSuccess,
      talent: `${game.i18n.localize('earthdawn.d.damage')} (${name})`,
      damageBonus: damageBonus2,
    };
    this.effectTest(inputs);
  }

  async effectTest(inputs) {
    let basestep = inputs.damagestep;
    let extraSuccess = 0;
    if (inputs.extraSuccess !== null) {
      extraSuccess = Number(inputs.extraSuccess * 2);
      if (inputs.damageBonus && inputs.damageBonus !== null) {
        extraSuccess += Number(inputs.damageBonus);
      }
    }

    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: `
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input id='dialog_box' class='misc-mod-input circleInput' value='${extraSuccess}' autofocus/>
            </div>
            <div class='misc-mod'>
                <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input id='karma_box' class='misc-mod-input circleInput' value='0' data-type='number' />
            </div>`,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    // let karmanew = this.data.data.karma.value - miscmod.karma;
    // V10 changes
    let karmanew = this.system.karma.value - miscmod.karma;
		// change End
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else if (miscmod.karma > 0) {
      karmaused = miscmod.karma;
    }

    inputs.finalstep = basestep + Number(miscmod.modifier);
    inputs.finalstep += damageTestModifiers(this);

    let karmastring = '';
    if (karmaused > 0) {
      // karmastring = `+${karmaused}${this.data.data.karmaDie}`;
      // V10 changes
      karmastring = `+${karmaused}${this.system.karmaDie}`;
		  // change End
    }
    let diceString = getDice(inputs.finalstep) + karmastring;
    let dice = explodify(diceString);
    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      damagetype: inputs.damagetype,
      rolltype: inputs.rolltype,
      talent: inputs.talent,
      finalstep: inputs.finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };

    inputs.dice = dice;
    inputs.name = this.name;

    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      // karmanew = this.data.data.karma.value - karmaused;
      // this.update({ 'data.karma.value': karmanew });
      // V10 changes
      karmanew = this.system.karma.value - karmaused;
      this.update({ 'system.karma.value': karmanew });
		// change End
      inputs.karmaused = karmaused;
    }
    return r.total;
  }

  async applyEffect(spellEffect){
    this.createEmbeddedDocuments("ActiveEffect", [spellEffect])
  }

  async parseSpell(spellName, extraSuccess) {
    // const spell = this.data.items.filter(function (item) {
    // V10 changes
    const spell = this.items.filter(function (item) {
    // change End
      return item.type === 'spell' && item.name === spellName;
    });
    // const spellEffect = spell[0].data.data.effect.toLowerCase();
    // V10 changes
    const spellEffect = spell[0].system.effect.toLowerCase();
		// change End
    let talent = 'Effect';
    let damagestep;
    let steps = 0;
    let modifier = 0;
    let damagetype;
    let rolltype = 'spellEffect';
    if (spellEffect.includes('wil')) {
      // steps = this.data.data.willpowerStep;
      // V10 changes
      steps = this.system.willpowerStep;
		  // change End
      if (spellEffect.includes('+')) {
        let regex = /([0-9])/g;
        let digitSearch = regex.exec(spellEffect);
        modifier = digitSearch[0];
      }
      if (spellEffect.includes('mystic') || spellEffect.includes('physical')) {
        rolltype = 'spelldamage';
        talent = 'Damage';
        if (spellEffect.includes('mystic')) {
          damagetype = 'mystic';
        } else if (spellEffect.includes('physical')) {
          damagetype = 'physical';
        }
      }
    }
    let willforce = game.i18n.localize('earthdawn.w.willforce');
    let willforceTalent = this.items.getName(willforce);

    damagestep = Number(steps) + Number(modifier);
    if (damagestep === 0) {
      ChatMessage.create({
        // content: `<div>Effect:</div><div>${spell[0].data.data.effect}</div>`,
        // V10 changes
        content: `<div>Effect:</div><div>${spell[0].system.effect}</div>`,
		    // change End
        speaker: ChatMessage.getSpeaker({ alias: this.name }),
      });
      return false;
    } else {
      if (this.items.getName(willforce)) {
        let miscmod = await new Promise((resolve) => {
          new Dialog({
            title: `game.i18n.localize("earthdawn.w.willforce")?`,
            content: `
                        <div>Do you want to use Willforce?</div>
                            `,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.y.yes'),
                callback: () => {
                  resolve({
                    willforce: true,
                  });
                },
              },
              no: {
                label: game.i18n.localize('earthdawn.n.no'),
                callback: () => {
                  resolve({
                    willforce: false,
                  });
                },
              },
            },
            default: 'ok',
          }).render(true);
        });
        if (miscmod.willforce === true) {
          // damagestep += willforceTalent.data.data.finalranks;
          // let newdamage = this.data.data.damage.value + 1;
          // this.update({ 'data.damage.value': newdamage });
          // V10 changes
          damagestep += willforceTalent.system.finalranks;
          let newdamage = this.system.damage.value + 1;
          this.update({ 'system.damage.value': newdamage });
		      // change End
        }
      }

      let inputs = {
        damagetype: damagetype,
        rolltype: rolltype,
        damagestep: damagestep,
        extraSuccess: extraSuccess,
        talent: `${talent} (${spellName})`,
      };
      this.effectTest(inputs);
    }
  }

  async rollToInitiative(inputs, r) {
    let array = Array.from(game.combat.combatants);
    //let activeCombatant = array.filter((combatant) => combatant.data.actorId === this.id);
    // V10 changes
    let activeCombatant = array.filter((combatant) => combatant.actorId === this.id);
		// change End
    let combatantID = activeCombatant[0].id;

    await game.combat.setInitiative(combatantID, r.total);
  }

  async NPCtest(inputs) {
    rollPrep (this, inputs)
  }

  async NPCDamage(actor, damage, extraSuccess) {
    let basestep = damage;
    extraSuccess = extraSuccess * 2;

    const html = `
    <div class='misc-mod'>
        <label class='misc-mod-description'>${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
        <input id='dialog_box' class='misc-mod-input circleInput' value='${extraSuccess}' autofocus/>
    </div>
    <div class='misc-mod'>
        <label class='misc-mod-description'>${game.i18n.localize('earthdawn.k.karma')}: </label>
        <input id='karma_box' class='misc-mod-input circleInput' value='0' data-type='number' />
    </div>`;
    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    //let karmanew = this.data.data.karma.value - miscmod.karma;
    // V10 changes
    let karmanew = this.system.karma.value - miscmod.karma;
		// change End
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else {
      karmaused = miscmod.karma;
    }
    let finalstep = Number(basestep) + Number(miscmod.modifier) + damageTestModifiers(this);
    let karmaUsedString = karmaused > 0 ? `+ ${karmaused}d6` : '';
    let diceString = getDice(finalstep) + karmaUsedString;
    let dice = explodify(diceString);

    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      type: 'effect',
      rolltype: 'weapondamage',
      talent: 'Damage',
      finalstep: finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };
    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      // karmanew = this.data.data.karma.value - karmaused;
      // this.update({ 'data.karma.value': karmanew });
      // V10 changes
      karmanew = this.system.karma.value - karmaused;
      this.update({ 'system.karma.value': karmanew });
		  // change End
      inputs.karmaused = karmaused;
    }
  }

  async takeDamage(inputs) {
    let damage = inputs.damage;
    let type = inputs.type;
    let ignorearmor = inputs.ignorearmor;
    let damagetaken;
    let newdamage;

    if (isNaN(damage)) {
      ui.notifications.error('No Damage Value Given');
      return false;
    } else {
      if (damage > 0 && type === 'physical') {
        if (ignorearmor === true) {
          damagetaken = damage;
        } else {
          // damagetaken = damage - this.data.data.physicalarmor;
          // V10 changes
          damagetaken = damage - this.system.physicalarmor;
		      // change End
        }
      } else if (damage > 0 && type === 'mystic') {
        if (ignorearmor === true) {
          damagetaken = damage;
        } else {
          // damagetaken = damage - this.data.data.mysticarmor;
          // V10 changes
          damagetaken = damage - this.system.mysticarmor;
		      // change End
        }
      } else {
        // damagetaken = damage - this.data.data.physicalarmor;
        // V10 changes
        damagetaken = damage - this.system.physicalarmor;
		    // change End
      }

      if (damagetaken < 0) {
        damagetaken = 0;
      }

      if (isNaN(damagetaken)) {
        ui.notifications.error('No Damage Value Given');
        return false;
      } else {
        // newdamage = Number(this.data.data.damage.value) + Number(damagetaken);
        // await this.update({ 'data.damage.value': newdamage });
        // V10 changes
        newdamage = Number(this.system.damage.value) + Number(damagetaken);
        await this.update({ 'system.damage.value': newdamage });
		    // change End
        let armormessage =
          ignorearmor === true ? `${game.i18n.localize('earthdawn.a.armorIgnored')}` : `${game.i18n.localize('earthdawn.a.armorApplied')}`;
        let html = `<div>${this.name} ${game.i18n.localize('earthdawn.t.took')} ${damagetaken} ${armormessage}</div>`;
        var newwounds;

        // if (this.data.data.damage.value >= this.data.data.damage.max) {
        // V10 changes
        if (this.system.damage.value >= this.system.damage.max) {
	    	// change End
          ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isDead')}`);
          html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isDead')}!</div>`;
          // if (damagetaken >= this.data.data.woundThreshold) {
          //   newwounds = this.data.data.wounds + 1;
          //   await this.update({ 'data.wounds': newwounds });
          // V10 changes
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds });
		      // change End
          }
        //} else if (this.data.data.damage.value >= this.data.data.unconscious.max) {
        // V10 changes
        } else if (this.system.damage.value >= this.system.unconscious.max) {
        // change End
          ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`);
          html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`;
          // if (damagetaken >= this.data.data.woundThreshold) {
          //   newwounds = this.data.data.wounds + 1;
          //   await this.update({ 'data.wounds': newwounds, 'data.tactics.knockeddown': true });
          // V10 changes
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds, 'system.tactics.knockeddown': true });
		      // change End
          }
        } else {
          // if (damagetaken >= this.data.data.woundThreshold) {
          //   newwounds = this.data.data.wounds + 1;
          //   await this.update({ 'data.wounds': newwounds });
          // V10 changes
          if (damagetaken >= this.system.woundThreshold) {
            newwounds = this.system.wounds + 1;
            await this.update({ 'system.wounds': newwounds });
		      // change End
              // if (damagetaken >= this.data.data.woundThreshold + 5) {
              //let knockdowndifficulty = damagetaken - this.data.data.woundThreshold;
              // V10 changes
              if (damagetaken >= this.system.woundThreshold + 5) {
              let knockdowndifficulty = damagetaken - this.system.woundThreshold;
		          // change End
              html += `<div>${this.name} ${game.i18n.localize('earthdawn.t.tookWound')}</div><div>${game.i18n.localize(
                'earthdawn.m.makeKnockDown',
              )} ${knockdowndifficulty}</div>`;
              let inputs = { difficulty: knockdowndifficulty };
              this.knockdownTest(inputs);
            }
          }
        }

        ChatMessage.create({
          content: html,
          speaker: ChatMessage.getSpeaker({ alias: this.name }),
        });
      }
    }
  }

  async recoveryTest() {
    // if (this.data.data.recoverytestscurrent < 1) {
    // V10 changes
    if (this.system.recoverytestscurrent < 1) {
    // change End
      ui.notifications.info(game.i18n.localize('earthdawn.r.recoveryNoLeft'));
      return false;
    } else {
      // let step = this.data.data.toughnessStep;
      // V10 changes
      let step = this.system.toughnessStep;
		  // change End
      let extraSuccesses = 0;
      let inputs = { damagestep: step, talent: 'Recovery Test', extraSuccess: extraSuccesses };
      let result = await this.effectTest(inputs);
      // result = Number(result) - this.data.data.woundsCurrent;
      // V10 changes
      console.log(this.system.woundsCurrent)
      result = Number(result) - this.system.woundsCurrent;
		  // change End
      if (result < 1) {
        result = 1;
      }
      //let newdamage = this.data.data.damage.value - result;
      // V10 changes
      console.log(this.system.damage.value)
      console.log(result)
      let newdamage = this.system.damage.value - result;
		  // change End
      newdamage = newdamage >= 0 ? newdamage : 0;
      // let newrecovery = this.data.data.recoverytestscurrent - 1;
      // this.update({ 'data.damage.value': newdamage, 'data.recoverytestscurrent': newrecovery });
      // V10 changes
      let newrecovery = this.system.recoverytestscurrent - 1;
      this.update({ 'system.damage.value': newdamage, 'system.recoverytestscurrent': newrecovery });
		  // change End
    }
  }

  woundCalc(wounds) {
    let resistPain = game.i18n.localize('earthdawn.r.resistpain');
    let fury = game.i18n.localize('earthdawn.f.fury');
    let finalwounds = wounds;
    //if (this.data.items.filter((item) => item.name.includes(resistPain)).length > 0 && wounds > 0) {
    // V10 changes
    if (this.items.filter((item) => item.name.includes(resistPain)).length > 0 && wounds > 0) {
    // change End
      ui.notifications.info('Character has Resist Pain (Calculated Automatically)');
      // let resistPainName = this.data.items.filter((item) => item.name.includes(resistPain))[0].name;
      // V10 changes
      let resistPainName = this.items.filter((item) => item.name.includes(resistPain))[0].name;
		  // change End
      let resistPainValue = Number(resistPainName.replace(/[^0-9]/g, ''));
      finalwounds = resistPainValue >= wounds ? 0 : Number(wounds) - Number(resistPainValue);
    }
    //if (this.data.items.filter((item) => item.name.includes(fury)).length > 0 && wounds > 0) {
      // V10 changes
      if (this.items.filter((item) => item.name.includes(fury)).length > 0 && wounds > 0) {
		  // change End
      ui.notifications.info('Character has Fury (Calculated Automatically)');
      // let furyName = this.data.items.filter((item) => item.name.includes(fury))[0].name;
      // V10 changes
      let furyName = this.items.filter((item) => item.name.includes(fury))[0].name;
		  // change End
      let furyValue = Number(furyName.replace(/[^0-9]/g, ''));
      if (wounds <= furyValue) {
        finalwounds = -wounds;
      }
    }
    return finalwounds;
  }

  async spellMatrixBox(inputs) {
    const label1 = game.i18n.localize('earthdawn.a.attune');
    const label2 = game.i18n.localize('earthdawn.m.matrixWeaveRed');
    const label3 = game.i18n.localize('earthdawn.m.matrixCastRed');
    const label4 = game.i18n.localize('earthdawn.m.matrixClearRed');
    const label5 = game.i18n.localize('earthdawn.s.spell');
    const label6 = game.i18n.localize('earthdawn.s.spellThreads');
    const matrix = this.items.get(inputs.matrixID);
    // const currentSpell = matrix.data.data.currentspell;
    // const currentThreads = matrix.data.data.activethreads;
    // V10 changes
    const currentSpell = matrix.system.currentspell;
    const currentThreads = matrix.system.activethreads;
		// change End

    const html = `<p>${label5}: ${currentSpell}</p>
    <p>${label6}: ${currentThreads}</p>
    <select name='spellFunction' id='spellFunction'>
    <option value='cast'>${label3}</option>
    <option value='attune'>${label1}</option>
    <option value='weave'>${label2}</option>
    <option value='clear'>${label4}</option>
    </select>`;

    let option = await new Promise((resolve) => {
      new Dialog({
        title: inputs.matrixName,
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                matrixFunction: html.find('#spellFunction').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    if (option.matrixFunction === 'attune') {
      this.attuneMatrix(matrix);
    } else if (option.matrixFunction === 'weave') {
      this.weaveThread(matrix);
    } else if (option.matrixFunction === 'cast') {
      this.castSpell(matrix);
    } else if (option.matrixFunction === 'clear') {
      this.clearMatrix(matrix);
    }
  }

  async newDay() {
    // if (this.data.data.damage.value > 0) {
    // V10 changes
    if (this.system.damage.value > 0) {
		// change End
      let updateRslt = await this.update({
        // 'data.karma.value': this.data.data.karma.max,
        // 'data.recoverytestscurrent': this.data.data.recoverytestsrefreshFinal,
        // V10 changes
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': this.system.recoverytestsrefreshFinal,
		    // change End
      });
      let testRslt = await this.recoveryTest();
      // announce recovery
      ui.notifications.info(game.i18n.localize('earthdawn.n.newDayRecovery'));
    // } else if (this.data.data.wounds > 0 && this.data.data.damage.value === 0) {
    //   let newwounds = this.data.data.wounds - 1;
    //   let newtests = this.data.data.recoverytestsrefreshFinal - 1;
    // V10 changes
    } else if (this.system.wounds > 0 && this.system.damage.value === 0) {
      let newwounds = this.system.wounds - 1;
      let newtests = this.system.recoverytestsrefreshFinal - 1;
		// change End
      this.update({
        // 'data.karma.value': this.data.data.karma.max,
        // 'data.recoverytestscurrent': newtests,
        // 'data.wounds': newwounds,
        // V10 changes
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': newtests,
        'system.wounds': newwounds,
		    // change End
      });
    } else {
      this.update({
        // 'data.karma.value': this.data.data.karma.max,
        // 'data.recoverytestscurrent': this.data.data.recoverytestsrefreshFinal,
        // V10 changes
        'system.karma.value': this.system.karma.max,
        'system.recoverytestscurrent': this.system.recoverytestsrefreshFinal,
		    // change End
      });
    }
  }
}
