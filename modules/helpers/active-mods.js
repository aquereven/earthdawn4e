import { capitalize } from '../helpers/capitalize.js';

export async function readMods(actor, inputs, activeMods = []) {
  if (inputs.steps) {
    activeMods.push(`Base Step: ${inputs.steps}`);
  } else {
    activeMods.push(`Attribute Ranks: ${inputs.basestep}`);
  }

  if (inputs.ranks > 0) {
    let typeName = capitalize(inputs.type);
    activeMods.push(`${typeName} Ranks: +${inputs.ranks}`);
  }
  // if (actor.data.data.wounds > 0) {
  //   activeMods.push(`Wounds: -${actor.data.data.wounds}`);
  // V10 changes
  if (actor.system.wounds > 0) {
    activeMods.push(`Wounds: -${actor.system.wounds}`);
  // change End
  }
  // if (actor.data.data.bonuses.allRollsStep > 0) {
  //   activeMods.push(`All Rolls (Active Effect): +${actor.data.data.bonuses.allRollsStep}`);
  // V10 changes
  if (actor.system.bonuses.allRollsStep > 0) {
    activeMods.push(`All Rolls (Active Effect): +${actor.system.bonuses.allRollsStep}`);
  // change End
  }

  // if (actor.data.data.tactics.knockeddown === true && inputs.rolltype !== 'jumpUp') {
  // V10 changes
  if (actor.system.tactics.knockeddown === true && inputs.rolltype !== 'jumpUp') {
  // change End
    activeMods.push(`Knocked Down: -3`);
  } else {
    // if (actor.data.data.tactics.defensive && inputs.rolltype !== 'knockdown') {
    // V10 changes
    if (actor.system.tactics.defensive && inputs.rolltype !== 'knockdown') {
		// change End
      activeMods.push(`Defensive Posture: -3`);
    // } else if (actor.data.data.tactics.aggressive === true && inputs.rolltype === 'attack' && inputs.attackType === 'closeAttack') {
    // V10 changes
    } else if (actor.system.tactics.aggressive === true && inputs.rolltype === 'attack' && inputs.attackType === 'closeAttack') {
		// change End
      activeMods.push(`Aggressive Stance: +3`);
    }
  }

  // if (actor.data.data.tactics.harried === true) {
  // V10 changes
  if (actor.system.tactics.harried === true) {
  // change End
    activeMods.push(`Harried: -2`);
  }

  if (inputs.rolltype === 'attack') {
    if (inputs.attackType === 'closeAttack') {
      activeMods.push(`Roll is a Close Combat Attack`);
      // if (actor.data.data.bonuses.closeAttack !== 0) {
      //   activeMods.push(`Close Attack (Active Effect): +${actor.data.data.bonuses.closeAttack}`);
      // V10 changes
      if (actor.system.bonuses.closeAttack !== 0) {
        activeMods.push(`Close Attack (Active Effect): +${actor.system.bonuses.closeAttack}`);
		  // change End
      }
    } else if (inputs.attackType === 'rangedAttack') {
      activeMods.push(`Roll is a Ranged Attack`);
    }
  }

  console.log(activeMods);
  return activeMods;
}
