export function chatListeners(html) {
  html.on('click', '.damageRoll', (ev) => {
    const actor = $(ev.currentTarget).attr('data-actorId');
    const weapon = $(ev.currentTarget).attr('data-weaponId');
    const extraSuccess = $(ev.currentTarget).attr('data-extraSuccess');
    const damageBonus = $(ev.currentTarget).attr('data-damageBonus');

    let actorData = game.actors.get(actor);
    let weaponData = actorData.items.get(weapon);


    actorData.weaponDamagePrep(weaponData, extraSuccess, damageBonus);
  });

  html.on('click', '.damageRollNPC', (ev) => {
    const actor = $(ev.currentTarget).attr('data-actorId');
    const damage = $(ev.currentTarget).attr('data-damagestep');
    const extraSuccess = $(ev.currentTarget).attr('data-extraSuccess');
    let actorData = game.actors.get(actor);

    actorData.NPCDamage(actorData, damage, extraSuccess);
  });

  html.on('click', '.applySpellEffect', (ev) => {
    const actorId = $(ev.currentTarget).attr('data-actorId');
    const spellId = $(ev.currentTarget).attr('data-spellId')
    let actor = game.actors.get(actorId);
    ui.notifications.info('Spell Id is: ' + spellId)
    let spell = actor.items.get(spellId)
    console.log(spell.effects)
    let spellEffects = Array.from(spell.effects);
    console.log(spellEffects)
    let finalEffect = spellEffects[0];
    socketlib.system.executeAsGM('effect', finalEffect);

  });


  html.on('click', '.apply-damage', async (ev) => {
    const targets = Array.from(game.user.targets);
    const damageType = $(ev.currentTarget).attr('data-damagetype');
    const damageSource = $(ev.currentTarget).attr('data-damagesource');

    let finalDamageType;
    let targetActor;
    let inputs = {};

    if (damageType === 'physical' || damageSource === 'weapondamage') {
      finalDamageType = 'physical';
    } else if (damageType === 'mystic') {
      finalDamageType = 'mystic';
    }
    const damageTotal = $(ev.currentTarget).attr('data-damagetotal');
    if (targets.length > 0) {
      targets.forEach(target => {
        targetActor = target.id;
        inputs = {
          damage: damageTotal,
          type: finalDamageType,
          targetActor: targetActor,
        };
        socketlib.system.executeAsGM('damage', inputs);
      });
    } else {
      ui.notifications.info('No Target Selected. Please Select a Target and Click Again');
    }
  });

  html.on('click', '.spellEffect', (ev) => {
    const actor = $(ev.currentTarget).attr('data-actorid');
    const spellName = $(ev.currentTarget).attr('data-spellname');
    const extraSuccess = $(ev.currentTarget).attr('data-extraSuccess');

    let actorData = game.actors.get(actor);

    actorData.parseSpell(spellName, extraSuccess);
  });
}
