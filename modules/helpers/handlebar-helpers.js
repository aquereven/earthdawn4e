export function registerHandlebarHelpers() {
  // Specific Handlebars
  Handlebars.registerHelper('PC', (context) => {
    return context.actor.type === 'pc';
  });

  Handlebars.registerHelper('NPC', (context) => {
    return context.actor.type === 'npc';
  });

  Handlebars.registerHelper('hasItems', (array) => {
    return array.length > 1;
  });

  Handlebars.registerHelper('getNamegiver', (context) => {
    const namegiver = context.items.find((item) => item.type === 'namegiver');
    console.log('[EARTHDAWN] Namegiver', namegiver);
    return namegiver;
  });

  Handlebars.registerHelper('findDiscipline', (array, type) => {
    // return array.find((item) => item.data.discipline === type);
    // V10 changes
    return array.find((item) => item.system.discipline === type);
		// change End
  });

  Handlebars.registerHelper('getDisciplines', (array) => {
    //return array.filter((item) => item.data.discipline === 'discipline');
    // V10 changes
    return array.filter((item) => item.system.discipline === 'discipline');
		// change End
  });

  Handlebars.registerHelper('getTalentSort', (sortType) => {
    return sortType === game.settings.settings.get('earthdawn4e.sortTalents').choices[game.settings.get('earthdawn4e', 'sortTalents')];
  });

  Handlebars.registerHelper('getSpellSort', (sortType) => {
    return sortType === game.settings.settings.get('earthdawn4e.sortSpells').choices[game.settings.get('earthdawn4e', 'sortSpells')];
  });

  Handlebars.registerHelper('getTalentType', (talents, type) => {
    // return talents.filter((talent) => talent.data.source === type);
    // V10 changes
    return talents.filter((talent) => talent.system.source === type);
		// change End
  });

  Handlebars.registerHelper('getCircleSpells', (circle, spells) => {
    // return spells.filter((spell) => spell.data.circle === circle);
    // V10 changes
    return spells.filter((spell) => spell.system.circle === circle);
		// change End
  });

  Handlebars.registerHelper('getSpellKnacks', (spell, knacks) => {
    if (spell === undefined || knacks === undefined) {
      return;
    }
    //return knacks.filter((knack) => knack.data.sourceTalentName === spell.name);
    // V10 changes
    return knacks.filter((knack) => knack.system.sourceTalentName === spell.name);
		// change End
  });

  Handlebars.registerHelper('getKnackName', (talent, knacks) => {
    if (talent === undefined || knacks === undefined) {
      return;
    }
    //return knacks.filter((knack) => knack.data.sourceTalentName === talent.name);
    // V10 changes
    return knacks.filter((knack) => knack.system.sourceTalentName === talent.name);
		// change End
  });

  Handlebars.registerHelper('highestCircle', (spells) => {
    let highestCircle = 0;
    spells.forEach((spell) => {
      // if (spell.data.circle > highestCircle) highestCircle = spell.data.circle;
      // V10 changes
      if (spell.system.circle > highestCircle) highestCircle = spell.system.circle;
		  // change End
    });

    let returnArray = [];
    for (let i = 0; i < highestCircle; i++) {
      returnArray.push(i + 1);
    }

    return returnArray;
  });

  Handlebars.registerHelper('attribute', (attributeStep) => {
    const stepMap = new Map();
    stepMap.set('dexterityStep', 'earthdawn.d.DEX');
    stepMap.set('strengthStep', 'earthdawn.s.STR');
    stepMap.set('toughnessStep', 'earthdawn.t.TOU');
    stepMap.set('perceptionStep', 'earthdawn.p.PER');
    stepMap.set('willpowerStep', 'earthdawn.w.WIL');
    stepMap.set('charismaStep', 'earthdawn.c.CHA');
    stepMap.set('initiativeStep', 'earthdawn.i.INI');
    stepMap.set('', 'earthdawn.n.NONE');
    return stepMap.get(attributeStep);
  });

  Handlebars.registerHelper('attributeName', (attributeNameStep) => {
    const stepMap = new Map();
    stepMap.set('dexterity', 'attributes.dexterityStep');
    stepMap.set('strength', 'attributes.strengthStep');
    stepMap.set('toughness', 'attributes.toughnessStep');
    stepMap.set('perception', 'attributes.perceptionStep');
    stepMap.set('willpower', 'attributes.willpowerStep');
    stepMap.set('charisma', 'attributes.charismaStep');
    stepMap.set('', 0);
    return stepMap.get(attributeNameStep);
  });

  Handlebars.registerHelper('getAttackType', (attacks, type) => {
    //return attacks.filter((attack) => attack.data.powerType === type);
    // V10 changes
    return attacks.filter((attack) => attack.system.powerType === type);
		// change End
  });

  Handlebars.registerHelper('getAttributeValue', (attributes, attribute) => {
    if (attribute === undefined || attribute === '') {
      return 0;
    }
    return attributes[attribute];
  });

  Handlebars.registerHelper('getSetting', (setting, type) => {
    return game.settings.settings.get(`earthdawn4e.${setting}`).choices[game.settings.get('earthdawn4e', setting)] === type;
  });

  Handlebars.registerHelper('DisciplineOrOther', (type, option) => {
    if (option === 'nav') {
      return type === 'discipline' ? 'earthdawn.c.circle' : 'earthdawn.r.rank';
    }
    if (option === 'talent-novice') {
      if (type === 'discipline') {
        return 'earthdawn.t.talentOptionsNovice';
      } else if (type === 'questor') {
        return 'earthdawn.d.devotionsFollowers';
      } else {
        return 'earthdawn.t.talentOptionsJourneyman';
      }
    } else if (option === 'talent-journeyman') {
      if (type === 'discipline') {
        return 'earthdawn.t.talentOptionsJourneyman';
      } else if (type === 'questor') {
        return 'earthdawn.d.devotionsAdherent';
      } else {
        return 'earthdawn.t.talentOptionsWarden';
      }
    } else if (option === 'talent-warden') {
      if (type === 'discipline') {
        return 'earthdawn.t.talentOptionsWarden';
      } else if (type === 'questor') {
        return 'earthdawn.d.devotionsExemplar';
      } else {
        return 'earthdawn.t.talentOptionsMaster';
      }
    } else if (option === 'talent-master') {
      if (type === 'discipline') {
        return 'earthdawn.t.talentOptionsMaster';
      }
    } else if ('talent-abilities') {
      if (type === 'discipline') {
        return 'earthdawn.d.disciplineTalent';
      } else if (type === 'questor') {
        return 'earthdawn.d.devotionAbilities';
      }
    }
  });

  Handlebars.registerHelper('PathOrQuestor', (disciplines) => {
    // const count = disciplines.filter((item) => item.data.discipline === 'path' || item.data.data.discipline === 'questor').length;
    // V10 changes
    const count = disciplines.filter((item) => item.system.discipline === 'path' || item.system.discipline === 'questor').length;
		// change End
    return count > 0;
  });

  Handlebars.registerHelper('NamegiverOrClass', (character) => {
    return character.disciplines.length > 0 || character.namegivers > 0;
  });

  Handlebars.registerHelper('hasDisciplineType', (disciplines, type) => {
    // const count = disciplines.filter((item) => item.data.discipline === type).length;
    // V10 changes
    const count = disciplines.filter((item) => item.system.discipline === type).length;
		// change End
    return count > 0;
  });

  // Generall Handlebars
  Handlebars.registerHelper('hasItems', (array) => {
    return array.length > 0;
  });

  Handlebars.registerHelper('inc', (value) => {
    return parseInt(value) + 1;
  });

  Handlebars.registerHelper('add', (value1, value2) => {
    return value1 + value2;
  });

  Handlebars.registerHelper('eq', (a, b) => {
    return a === b;
  });

  Handlebars.registerHelper('uneq', (a, b) => {
    return a !== b;
  });
}
