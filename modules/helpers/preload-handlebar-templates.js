export function preloadHandlebarTemplates() {
  const templatePaths = [
    // Actor Partials
    'systems/earthdawn4e/templates/actors/partials/characteristics-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/talents-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/spells-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/inventory-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/combat-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/story-notes-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/talent-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/talent-card-short.hbs',
    'systems/earthdawn4e/templates/actors/partials/equipment-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/shield-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/skill-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/spell-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/weapon-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/discipline-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/attack-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/matrix-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/armor-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/spell-list.hbs',
    'systems/earthdawn4e/templates/actors/partials/thread-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/creature-characteristic-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/creature-combat-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/creature-information-tab.hbs',
    'systems/earthdawn4e/templates/actors/partials/knack-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/devotion-card.hbs',
    'systems/earthdawn4e/templates/actors/partials/devotion-card-short.hbs',
    'systems/earthdawn4e/templates/actors/partials/effects-card.hbs',


    // Item Partials
    'systems/earthdawn4e/templates/items/partials/attribute-options.hbs',
    'systems/earthdawn4e/templates/items/partials/threadTalentOptions-card.hbs',
    'systems/earthdawn4e/templates/items/partials/modifier-list.hbs',
    'systems/earthdawn4e/templates/items/partials/thread-item-characteristics.hbs',
    'systems/earthdawn4e/templates/items/partials/thread-items-tab.hbs',
    'systems/earthdawn4e/templates/items/partials/thread-item.hbs',
    'systems/earthdawn4e/templates/items/partials/item-description-tab.hbs',
    'systems/earthdawn4e/templates/items/partials/effects-tab.hbs',
  ];

  const takaThemePaths = [
    // Actor Partials
    // General
    'systems/earthdawn4e/templates/taka-theme/actors/partials/creature-property-list.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/property-list.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/header-attributes.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/item-control.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/header-attributes.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/item-control-effects.hbs',

    // Tabs
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tabs/biography-tab.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tabs/creature-information-tab.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tabs/creature-combat-tab.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tabs/combat-tab.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tabs/inventory-tab.hbs',

    // Table
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/armors-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/equipment-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/matrixes-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/shields-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/skills-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/devotions-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/spells-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/talents-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/weapons-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/thread-table.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/tables/effects-table.hbs',

    // Cards
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/attack-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/armor-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/devotion-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/equipment-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/knack-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/matrix-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/shield-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/skill-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/spell-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/talent-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/talent-card-short.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/weapon-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/thread-card.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/cards/effects-card.hbs',

    // Selects
    'systems/earthdawn4e/templates/taka-theme/actors/partials/selects/select-karma-dice.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/selects/select-devotion-dice.hbs',
    'systems/earthdawn4e/templates/taka-theme/actors/partials/selects/select-use-karma.hbs',

    // Item Partials
    // General
    'systems/earthdawn4e/templates/taka-theme/items/partials/item-sheet-header.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/discipline-sheet-header.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/spell-details.hbs',

    // Tabs
    'systems/earthdawn4e/templates/taka-theme/items/partials/tabs/thread-tab.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/tabs/effects-tab.hbs',

    // Navs
    'systems/earthdawn4e/templates/taka-theme/items/partials/navs/basic-nav.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/navs/inventory-nav.hbs',

    // Sidebars
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/armor-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/attack-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/devotion-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/discipline-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/equipment-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/knack-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/namegiver-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/shield-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/skill-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/spell-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/talent-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/item-thread-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/thread-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/weapon-sidebar.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/sidebars/spellmatrix-sidebar.hbs',

    // Selects
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-availability.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-attribute.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-action.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-devotion-required.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-discipline.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-favorite.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-power-type.hbs',
    'systems/earthdawn4e/templates/taka-theme/items/partials/selects/select-characteristics.hbs',
  ];

  loadTemplates(templatePaths.concat(takaThemePaths));
}
