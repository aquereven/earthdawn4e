// export function defenseModifiers(actor) {
//   let mod = 0;
//   /* a knocked down character cannot use any combat options with the exception of jump up */
//   if (actor.system.tactics.knockeddown === true) {
//     mod -= 3;
//   } else {
//     if (actor.system.tactics.aggressive === true) {
//       mod -= 3;
//     } else if (actor.system.tactics.defensive === true) {
//       mod += 3;
//     }
//   }

//   if (actor.system.tactics.harried === true) {
//     mod -= 2;
//   }

//   return mod;
// }

// export function actionTestModifiers(actor, rolltype, closeCombat) {
//   let mod = 0;
//   let strain = 0;
//   /* a knocked down character cannot use any combat options with the exception of jump up */
//   if (actor.system.tactics.knockeddown === true && rolltype !== 'jumpUp') {
//     mod -= 3;
//   } else {
//     if (actor.system.tactics.defensive && rolltype !== 'knockdown') {
//       mod -= 3;
//     } else if (actor.system.tactics.aggressive === true && rolltype === 'attack' && closeCombat) {
//       mod += 3;
//       strain += 1;
//     }
//   }

//   if (actor.system.tactics.harried === true) {
//     mod -= 2;
//   }

//   if (rolltype === "attack" && closeCombat){
//     mod += actor.system.bonuses.closeAttack + actor.system.bonuses.allAttack;
//   }
//   else if (rolltype === "attack" && !closeCombat){
//     mod += actor.system.bonuses.rangedAttack + actor.system.bonuses.allAttack;
//   }

//   return {modifiers: mod, strain: strain};
// }

// export function damageTestModifiers(actor) {
//   let mod = 0;

//   if (actor.system.tactics.aggressive) {
//     mod += 3;
//   } else if (actor.system.tactics.defensive) {
//     mod -= 3;
//   }

//   return mod;
// }


// V10 changes
export function defenseModifiers(actor) {
  let mod = 0;
  /* a knocked down character cannot use any combat options with the exception of jump up */
  if (actor.system.tactics.knockeddown === true) {
    mod -= 3;
  } else {
    if (actor.system.tactics.aggressive === true) {
      mod -= 3;
    } else if (actor.system.tactics.defensive === true) {
      mod += 3;
    }
  }

  if (actor.system.tactics.harried === true) {
    mod -= 2;
  }

  return mod;
}

export function actionTestModifiers(actor, rolltype, closeCombat) {
  let mod = 0;
  let strain = 0;
  /* a knocked down character cannot use any combat options with the exception of jump up */
  if (actor.system.tactics.knockeddown === true && rolltype !== 'jumpUp') {
    mod -= 3;
  } else {
    if (actor.system.tactics.defensive && rolltype !== 'knockdown') {
      mod -= 3;
    } else if (actor.system.tactics.aggressive === true && rolltype === 'attack' && closeCombat) {
      mod += 3;
      strain += 1;
    }
  }

  if (actor.system.tactics.harried === true) {
    mod -= 2;
  }

  if (rolltype === "attack" && closeCombat){
    mod += actor.system.bonuses.closeAttack + actor.system.bonuses.allAttack;
  }
  else if (rolltype === "attack" && !closeCombat){
    mod += actor.system.bonuses.rangedAttack + actor.system.bonuses.allAttack;
  }

  return {modifiers: mod, strain: strain};
}

export function damageTestModifiers(actor) {
  let mod = 0;

  if (actor.system.tactics.aggressive) {
    mod += 3;
  } else if (actor.system.tactics.defensive) {
    mod -= 3;
  }

  return mod;
}

export function initiativeMod(actor)
{
  let mod = 0;
  if (actor.system.tactics.harried === true){
    mod -= 2;
  }
  if (actor.system.tactics.defensive) {
      mod -= 3;
    }
    if (actor.system.tactics.knockeddown) {
      mod -= 3;
    }
    return mod;
  }


// change End