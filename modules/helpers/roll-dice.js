import {chatOutput} from '../helpers/chat-message.js';
import {explodify} from '../helpers/explodify.js'

export async function rollDice(actor, inputs) {
    console.log(actor)
    console.log('[EARTHDAWN] Roll Inputs', inputs);

    let dice = actor.getDice(inputs.finalstep);
    inputs.karmaUsed = inputs.karma ? inputs.karma : 0;
    //devotion - what is actor doing exactly?
    inputs.devotionUsed = inputs.devotion ? inputs.devotion : 0;
    let extraSuccess = 0;
    let karmaDie =
    //   actor.data.data.karmaDie === 'S8'
    //     ? '2d6'
    //     : actor.data.data.karmaDie === 'S9'
    //     ? 'd8+d6'
    //     : actor.data.data.karmaDie === 'S10'
    //     ? '2d8'
    //     : actor.data.data.karmaDie;
    // let devotionDie = actor.data.data.devotionDie;
    // if (inputs.karmaUsed > 0 && inputs.devotionUsed == 0) {
    //   let karmaNew = actor.data.data.karma.value - inputs.karmaUsed;

        // V10 changes
        actor.system.karmaDie === 'S8'
          ? '2d6'
          : actor.system.karmaDie === 'S9'
          ? 'd8+d6'
          : actor.system.karmaDie === 'S10'
          ? '2d8'
          : actor.system.karmaDie;
        let devotionDie = actor.system.devotionDie;
        if (inputs.karmaUsed > 0 && inputs.devotionUsed == 0) {
        let karmaNew = actor.system.karma.value - inputs.karmaUsed;
        // change End
      if (karmaNew < 0) {
        ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
        return false;
      }
      let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
      let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

      //Dice creation - Karma only
      dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}`;
    } /*check for devotion */ else if (inputs.devotionUsed > 0 && inputs.karmaUsed == 0) {
      // let devotionNew = actor.data.data.devotion.value - inputs.devotionUsed;
      // if (actor.data.data.devotion.max > 0 && devotionNew < 0) {

      // V10 changes
      let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
      if (actor.system.devotion.max > 0 && devotionNew < 0) {
      // change End
        ui.notifications.info(game.i18n.localize('earthdawn.d.devotionNo'));
        return false;
      }
      // Dice creation - Devotion only
      dice += `+` + inputs.devotionUsed + inputs.devotionDie;
    } /* check for devotion and karma */ else if (inputs.karmaUsed > 0 && inputs.devotionUsed > 0) {
      // let karmaNew = actor.data.data.karma.value - inputs.karmaUsed;
      // let devotionNew = actor.data.data.devotion.value - inputs.devotionUsed;
      // V10 changes
      let karmaNew = actor.system.karma.value - inputs.karmaUsed;
      let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
      // change End
      if (karmaNew < 0) {
        ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
        return false;
      }
      // if (actor.data.data.devotion.max > 0 && devotionNew < 0) {
        // V10 changes
        if (actor.system.devotion.max > 0 && devotionNew < 0) {
        // change End
        ui.notifications.info(game.i18n.localize('earthdawn.d.devotionNo'));
        return false;
      }
      let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
      let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

      // Dice creation - Devotion and Karma
      dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}` + `+` + inputs.devotionUsed + inputs.devotionDie;
    }
    let formula = explodify(dice);
    let r = new Roll(`${formula}`);

    await r.evaluate();

    console.log('[EARTHDAWN] Roll', r);

    if (r.roll) {
      if (inputs.karmaUsed > 0) {
        // let karmaNew = actor.data.data.karma.value - inputs.karmaUsed;
        // await actor.update({ 'data.karma.value': karmaNew });
        // V10 changes
        let karmaNew = actor.system.karma.value - inputs.karmaUsed;
        await actor.update({ 'system.karma.value': karmaNew });
        // change End
      }

      //Remove devotion input value of option Box from current devotion points.
      if (inputs.devotionUsed > 0) {
        // let devotionNew = actor.data.data.devotion.value - inputs.devotionUsed;
        // await actor.update({ 'data.devotion.value': devotionNew });
        // V10 changes
        let devotionNew = actor.system.devotion.value - inputs.devotionUsed;
        await actor.update({ 'system.devotion.value': devotionNew });
        // change End
      }

      if (inputs.strain > 0) {
        // let damageNew = Number(actor.data.data.damage.value) + Number(inputs.strain);
        // await actor.update({ 'data.damage.value': damageNew });
        // V10 changes
        let damageNew = Number(actor.system.damage.value) + Number(inputs.strain);
        await actor.update({ 'system.damage.value': damageNew });
        // change End
      }
    }

    if (inputs.ammoDecrement){
      console.log(inputs.ammoId);
      let ammoItem = actor.items.get(inputs.ammoId);
      console.log (ammoItem.system.amount)
      let amountNew = Number(ammoItem.system.amount - 1);
      console.log(amountNew)
      await ammoItem.update({'system.amount': amountNew}) 
    }


    if (inputs.difficulty > 0) {
      let margin = r.total - inputs.difficulty;
      inputs.margin = margin;
      inputs.extraSuccess = margin > 0 ? Math.floor(margin / 5) : 0;
    } else {
      inputs.extraSuccess = 0;
    }

    inputs.dice = dice;
    inputs.name = actor.name;
    if (inputs.initiative === true) {
      actor.rollToInitiative(inputs, r);
    }

    chatOutput(inputs, r);
    return inputs;
}