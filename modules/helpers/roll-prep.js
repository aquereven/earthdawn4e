import { karmaAllowed } from '../helpers/karma-allowed.js';
import { devotionAllowed } from '../helpers/devotion-allowed.js';
import { getOptionBox } from '../helpers/option-box.js';
import { readMods } from '../helpers/active-mods.js';
import { actionTestModifiers, defenseModifiers, damageTestModifiers, initiativeMod } from '../helpers/combat-modifiers.js';
import {rollDice} from '../helpers/roll-dice.js';

export async function rollPrep(actor, inputs) {
  console.log('[EARTHDAWN] Roll Prep Input', inputs);

  let activeMods = [];

  console.log(actor);

  const weapon = inputs.weaponID ? actor.items.get(inputs.weaponID) : null;
  // const attackTalentID = inputs.attackTalentID ? inputs.attackTalentID : weapon ? await actor.getAttack(weapon.data.data.weapontype) : null;
  // V10 changes
  const attackTalentID = inputs.attackTalentID ? inputs.attackTalentID : weapon ? await actor.getAttack(weapon.system.weapontype) : null;
	// change End
  const damageTalent = inputs.damageTalentID ? inputs.damageTalentID : null;
  const item = inputs.talentID ? actor.items.get(inputs.talentID) : attackTalentID ? actor.items.get(attackTalentID) : null;
  // const att = inputs.attribute ? inputs.attribute : item ? item.data.data.attribute : weapon ? 'dexterityStep' : null;
  // V10 changes
  const att = inputs.attribute ? inputs.attribute : item ? item.system.attribute : weapon ? 'dexterityStep' : null;
	// change End
  const modifier = inputs.modifier ? inputs.modifier : 0;
  const targets = inputs.targets ? inputs.targets : null;
  const ranks = inputs.ranks
    ? inputs.ranks
    : item
    // ? item.data.data.finalranks
    //   ? item.data.data.finalranks
    //   : item.data.data.ranks
    //   ? item.data.data.ranks
    // V10 changes
    ? item.system.finalranks
    ? item.system.finalranks
    : item.system.ranks
    ? item.system.ranks
		// change End
     : 0
    : 0;
  const title = inputs.talentName
    ? inputs.talentName
    : item
    ? item.name
    : inputs.talent
    ? inputs.talent
    : game.i18n.localize(`earthdawn.${att.slice(0, 1)}.${att.slice(0, -4)}`);
  const type = item ? item.type : '';
  const rolltype = inputs.rolltype ? inputs.rolltype : inputs.type === "attack" ? "attack" : '';
  const karma = inputs.karmaNumber ? inputs.karmaNumber : 0;
  const damageBonus = inputs.damageBonus ? inputs.damageBonus : 0;
  const steps = inputs.steps ? inputs.steps : null;
  //devotion
  const devotion = inputs.devotionNumber ? inputs.devotionNumber : 0;
  const initiative = inputs.initiative ? inputs.initiative : false;
  const difficulty = inputs.difficulty ? inputs.difficulty : rolltype === 'attack' ? await actor.targetDifficulty() : 0;
  const strain =
    (inputs.strain ? Number(inputs.strain) : 0) +
    //(item != null ? Number(item.data.data.strain) : 0) +
    // (actor.data.data.tactics.aggressive === true &&
    // V10 changes
    (item != null ? Number(item.system.strain) : 0) +
    (actor.system.tactics.aggressive === true &&
		// change End
    rolltype === 'attack' &&
    // (weapon.data.data.weapontype === 'Melee' || weapon.data.data.weapontype === 'Unarmed')
    // V10 changes
    (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed')
		// change End
      ? 1
      : 0);
  const spellName = inputs.spellName ? inputs.spellName : '';
  const spellId = inputs.spellId ? inputs.spellId : '';
  const damageId = damageTalent ? damageTalent : weapon ? inputs.weaponID : null;
  //let wounds = actor.data.data.wounds;
  // V10 changes
  let wounds = actor.system.wounds;
	// change End
  wounds = actor.woundCalc(wounds);
  let karmarequested;
  if (karma > 0) {
    karmarequested = 'true';
  } else {
    // karmarequested = actor.data.data.usekarma;
    // V10 changes
    karmarequested = actor.system.usekarma;
		// change End
  }

  let devotionrequested;
  if (devotion > 0) {
    devotionrequested = 'true';
  } else {
    devotionrequested = 'true';
  }

  let devotionDie;
  // if (actor.data.data.devotion.max !== 0) {
  //   devotionDie = actor.data.data.devotionDie;
  // } else if (actor.data.data.devotion.max === 0) {
  // V10 changes
  if (actor.system.devotion.max !== 0) {
    devotionDie = actor.system.devotionDie;
  } else if (actor.system.devotion.max === 0) {
  // change End
    devotionDie = 'd4';
  }

  // let basestep = actor.data.data[att];
  // V10 changes
  let basestep = actor.system[att] ? actor.system[att]: 0 ;
	// change End
  if (!basestep && !inputs.steps) {
    ui.notifications.error(game.i18n.localize('earthdawn.a.attributeNotAssigned'));
  }

  inputs = {
    talent: title,
    actorId: actor.id,
    type: type,
    modifier: modifier,
    attribute: att,
    basestep: basestep,
    ranks: ranks,
    karmarequested: karmarequested,
    devotionrequested: devotionrequested,
    devotionDie: devotionDie,
    strain: strain,
    difficulty: difficulty,
    weaponId: damageId,
    rolltype: rolltype,
    spellName: spellName,
    spellId: spellId,
    steps: steps,
    title: title,
    initiative: initiative,
    damageBonus: damageBonus,
    targets: targets
  };
  if (rolltype === 'attack' && weapon) {
    console.log(weapon.system.weapontype)
    inputs.attackType =
      // weapon.data.data.weapontype === 'Melee' || weapon.data.data.weapontype === 'Unarmed'
      // V10 changes
      (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed')
		  // change End
        ? 'closeAttack'
        // : weapon.data.data.weapontype === 'Ranged' || weapon.data.data.weapontype === 'Thrown'
        // V10 changes
        : (weapon.system.weapontype === 'Ranged' || weapon.system.weapontype === 'Thrown')
		    // change End
        ? 'rangedAttack'
        : null;
  }

  if (game.settings.get('earthdawn4e', 'trackAmmo')){
    let ammoType;
    if (inputs.attackType === "rangedAttack" && weapon.system.weapontype === "Ranged"){
      inputs.ammoDecrement = true;
      let weaponName = weapon.name.toLowerCase();
      console.log(weaponName)
      if ( weaponName.includes(game.i18n.localize('earthdawn.c.crossbow'))){
        ammoType = "b.bolt"
      }
      else if (weaponName.includes(game.i18n.localize('earthdawn.b.bow'))){
        ammoType = "a.arrow"
      }
      else if (weaponName.includes(game.i18n.localize('earthdawn.s.sling'))){
        ammoType = "s.stone";
      }
      else if (weaponName.includes(game.i18n.localize('earthdawn.b.blowgun'))){
        ammoType = "n.needle";
      }
      let ammoName = game.i18n.localize (`earthdawn.${ammoType}`)
      let lowerAmmoName = ammoName.toLowerCase();
      
      let ammoId = actor.items.filter(function (item) {
        let lowerName = item.name.toLowerCase();
        return lowerName.includes(game.i18n.localize(lowerAmmoName));
      });
      console.log(ammoId)
      if (ammoId.length === 0 || Number(ammoId[0].system.amount) < 1){
        ui.notifications.error("No Ammo!");
        return;
      }
      inputs.ammoId = ammoId[0]._id;
      console.log(ammoId[0]._id)

      inputs.ammoDecrement = true;
    
    }
    else {
      inputs.ammoDecrement = false;
    }
  }
 

  if (karma > 0) {
    inputs.karma = karma;
  } else {
    inputs.karma = karmaAllowed(inputs);
  }
  //devotion
  if (devotion > 0) {
    inputs.devotion = devotion;
  } else {
    inputs.devotion = devotionAllowed(inputs);
  }

  activeMods = await readMods(actor, inputs, activeMods);

  //let miscmod = await optionBox(inputs, activeMods);
  let miscmod = await getOptionBox(inputs, activeMods);
  if (miscmod.cancel) {
    miscmod = {};
    return;
  }

  inputs.karma = miscmod.karma;
  inputs.devotion = miscmod.devotion;
  inputs.devotionDie = miscmod.devotionDie;
  inputs.difficulty = miscmod.difficulty;
  inputs.modifier = miscmod.modifier;
  //finalstep is set using the step if the actor is a Creature, and using the Attribute + Ranks if its not
  // console.log(actor.data.data.bonuses.allRollsStep);
  // V10 changes
  console.log(actor.system.bonuses.allRollsStep);
	// change End
  if (steps !== null) {
    // inputs.finalstep = Number(steps) + Number(miscmod.modifier) - Number(wounds) + actor.data.data.bonuses.allRollsStep;
    // V10 changes
    console.log("Using Steps")
    inputs.finalstep = Number(steps) + Number(miscmod.modifier) - Number(wounds) + actor.system.bonuses.allRollsStep;
    console.log(inputs.finalstep)
		// change End
  } else {
    inputs.finalstep =
      // Number(basestep) + Number(inputs.ranks) + Number(miscmod.modifier) - Number(wounds) + actor.data.data.bonuses.allRollsStep;
      // V10 changes
      Number(basestep) + Number(inputs.ranks) + Number(miscmod.modifier) - Number(wounds) + actor.system.bonuses.allRollsStep;
		  // change End
  }

  inputs.strain = miscmod.strain;

  let mods = actionTestModifiers(
    actor,
    inputs.rolltype,
    // rolltype === 'attack' && (weapon.data.data.weapontype === 'Melee' || weapon.data.data.weapontype === 'Unarmed') ? true : false,
    // V10 changes
    rolltype === 'attack' && weapon && (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed') ? true : false,
		// change End
  );
  

  //No Step roll can be less than 1
  console.log(inputs.finalstep)
  inputs.finalstep = Number(inputs.finalstep) > 0 ? Number(inputs.finalstep) : 1;
  console.log("Final Step is " + inputs.finalstep)

  console.log('[EARTHDAWN] Roll Prep Outputs', inputs);
  return await rollDice(actor, inputs);
}
