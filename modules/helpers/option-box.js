export async function getOptionBox(inputs, activeMods = []) {
  inputs.activeMods = activeMods;
  return new Promise((resolve) => {
    let OptBox = new OptionBox(inputs, resolve);
    OptBox.render(true);
  });
}
export class OptionBox extends Application {
  constructor(inputs, resolve, tabs) {
    super({
      title: inputs.title ? inputs.title : game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
    });
    this.inputs = inputs;
    this.activeMods = inputs.activeMods;
    this.resolve = resolve;
    this.tabs = tabs;
  }
  

  activateListeners(html) {
    html.find("[data-button='cancel']").click((ev) => {
      return this._onCancel();
    });

    html.find("[data-button='ok']").click((ev) => {
      return this._onOK();
    });
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['optionBox', 'dialog'],
      template: `systems/earthdawn4e/templates/popups/option-box.hbs`,
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'inputfields',
        },
      ],
      width: 400,
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/popups/option-box.hbs`;
  }

  /** @override */
  getData() {
    return { inputs: this.inputs };
  }

  submit() {
    this.render(true);
  }

  _onCancel() {
    //this.resolve({cancel: true});
    this.close();
  }

  _onOK() {
    this.resolve({
      modifier: this.element.find('#dialog_box').val(),
      difficulty: this.element.find('#difficulty_box').val(),
      strain: this.element.find('#strain_box').val(),
      karma: this.element.find('#karma_box').val(),
      devotion: this.element.find('#devotion_box').val(),
      devotionDie: this.element.find('#devotionDie_box').val(),
    });
    this.close();
  }

  _onKeyDown(event) {
    // close dialog
    if (event.key === 'Escape') {
      event.preventDefault();
      event.stopPropagation();
      return this.close();
    }
    // confirm default choice
    if (event.key === 'Enter') {
      event.preventDefault();
      event.stopPropagation();
      this.onOK();
    }
  }

  async close(options = {}) {
    this.resolve({ cancel: true });
    return super.close();
  }
}
