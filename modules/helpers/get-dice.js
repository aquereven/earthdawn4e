import { get1eDice } from './get-1e-dice.js';
import { get3eDice } from './get-3e-Dice.js';
import { getCeDice } from './get-ce-dice.js';

export function getDice(step) {
  
  let dice = 0;
    if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step4') {
      var stepTable = [
        '0',
        '1d4-2',
        '1d4-1',
        '1d4',
        '1d6',
        '1d8',
        '1d10',
        '1d12',
        '2d6',
        '1d8+1d6',
        '2d8',
        '1d10+1d8',
        '2d10',
        '1d12+1d10',
        '2d12',
        '1d12+2d6',
        '1d12+1d8+1d6',
        '1d12+2d8',
        '1d12+1d10+1d8',
        '1d20+2d6',
        '1d20+1d8+1d6',
      ];
      if (step < 0) {
        return;
      } else if (step < 19) {
        dice = stepTable[step];
      } else {
        let i = step;
        let loops = 0;
        while (i > 18) {
          loops += 1;
          i -= 11;
        }
        dice = loops + 'd20+' + stepTable[i];
      }
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step1') {
      dice = get1eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step3') {
      dice = get3eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'stepC') {
      dice = getCeDice(step);
    }
    return dice;
  }
