export default class Earthdawn4eActorSheet extends ActorSheet {
  takeDamageDialog = `
          <div style="float: left">
              <label>${game.i18n.localize('earthdawn.d.damage')}: </label>
              <input id="damage_box" value=0 autofocus/>
          </div>
          <div>
              <label>${game.i18n.localize('earthdawn.t.type')}: </label>
              <select id="type_box">
                <option value="physical">Physical</option>
                <option value="mystic">Mystic</option>
              </select>
          </div>
          <div>
            <label>${game.i18n.localize('earthdawn.i.ignoreArmor')}?</label>
            <input type="checkbox" id="ignore_box"/>
          </div>`;

  baseListeners(html) {
    super.activateListeners(html);

    $(document).on('keydown', 'form', function (ev) {
      return ev.key !== 'Enter';
    });

    html.find('.item-delete').click(async (ev) => {
      let li = $(ev.currentTarget).parents('.item-name'),
        itemId = li.attr('data-item-id');
      let confirmationResult = await this.confirmationBox();
      if (confirmationResult.result === false) {
        return false;
      } else {
        this.actor.deleteEmbeddedDocuments('Item', [itemId]);
      }
    });

    html.find('.item-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // V10 changes
      const item = this.actor.items.get(li.data('itemId'));
		  // change End
      item.sheet.render(true);
    });

    html.find('.link-checkbox-effect').click(async (ev) => {
      ev.preventDefault();

      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.effects.get(li.data('itemId'));
      // V10 changes
      const item = this.actor.effects.get(li.data('itemId'));
		  // change End
      let visibleState = ev.target.checked;
      let disabledState = !visibleState;

      await item.update({ disabled: disabledState });
    });

    html.find('.effect-delete').click(async (ev) => {
      let li = $(ev.currentTarget).parents('.item-name'),
        itemId = li.attr('data-item-id');
      let confirmationResult = await this.confirmationBox();
      if (confirmationResult.result === false) {
        return false;
      } else {
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [itemId]);
      }
    });

    html.find('.effect-add').click(() => {

      // let itemNumber = this.actor.data.effects.size;
      // V10 changes
      let itemNumber = this.actor.effects.size;
		  // change End
      let itemData = {
        label: `New Effect ` + itemNumber,
        icon: 'systems/earthdawn4e/assets/effect.png',
        duration: { rounds: 1 },
        origin: this.actor.id,
      };

      this.actor.createEmbeddedDocuments('ActiveEffect', [itemData]);
    });

    html.find('.effect-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.effects.get(li.data('itemId'));
      // V10 changes
      const item = this.actor.effects.get(li.data('itemId'));
		  // change End
      item.sheet.render(true);
    });

    html.find('.NPC-attack').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // let itemID = li.data('itemId');
      // V10 changes
      const item = this.actor.items.get(li.data('itemId'));
      let itemID = li.data('itemId');
		  // change End
      const modifier = 0;
      // const strain = item.data.data.strain ? item.data.data.strain : 0;
      // V10 changes
      const strain = item.system.strain ? item.system.strain : 0;
		  // change End
      const karma = 0;

      // let type = item.data.data.powerType === 'Attack' ? 'attack' : item.data.data.attackstep > 0 ? 'test' : '';
      // V10 changes
      let type = item.system.powerType === 'Attack' ? 'attack' : item.system.attackstep > 0 ? 'test' : '';
		  // change End
      const parameters = {
        itemID: itemID,
        // steps: item.data.data.attackstep,
        // V10 changes
        steps: item.system.attackstep,
		    // change End
        talent: item.name,
        strain: strain,
        type: type,
        karma: karma,
        weaponID: item.id,
        modifier: modifier,
      };
      this.actor.rollPrep(parameters);
    });

    html.find('.knockdown-test').click(() => {
      let inputs = {};
      this.actor.knockdownTest(inputs);
    });

    html.find('.jump-up').click(() => {
      this.actor.jumpUpTest();
    });

    html.find('.take-damage').click(async () => {
      let inputs = await new Promise((resolve) => {
        new Dialog({
          title: game.i18n.localize('earthdawn.t.takeDamage'),
          content: this.takeDamageDialog,
          buttons: {
            ok: {
              label: game.i18n.localize('earthdawn.o.ok'),
              callback: (html) => {
                resolve({
                  damage: html.find('#damage_box').val(),
                  type: html.find('#type_box').val(),
                  ignore: html.find('#ignore_box:checked'),
                });
              },
            },
          },
          default: 'ok',
        }).render(true);
      });

      inputs.ignorearmor = inputs.ignore.length > 0;
      this.actor.takeDamage(inputs);
    });

    html.find('.item-display').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // V10 changes
      const item = this.actor.items.get(li.data('itemId'));
		  // change End

      let html = `
                <img src="${item.img}" title="${item.name}" height="64" width="64"/>
                <div>Name: ${item.name}</div>`;

      // if (item.data.data.damagestep) {
      //   html += `<div>Damage Step: ${item.data.data.damagestep}</div>`;
      // V10 changes
      if (item.system.damagestep) {
        html += `<div>Damage Step: ${item.system.damagestep}</div>`;
		  // change End
      }

      // if (item.data.data.damagestep) {
      //   html += `<div>Damage Step: ${item.data.data.damagestep}</div>`;
      // V10 changes
      if (item.system.damagestep) {
        html += `<div>Damage Step: ${item.system.damagestep}</div>`;
		  // change End
      }

      // html += `<div>${item.data.data.description}</div>`;
      // V10 changes
      html += `<div>${item.system.description}</div>`;
		  // change End

      const chatData = {};
      let rollMode = game.settings.get('core', 'rollMode');

      if (['gmroll', 'blindroll'].includes(rollMode)) chatData['whisper'] = ChatMessage.getWhisperRecipients('GM');
      if (rollMode === 'blindroll') chatData['blind'] = true;

      ChatMessage.create({
        content: html,
        whisper: chatData.whisper,
        blind: chatData.blind,
        speaker: ChatMessage.getSpeaker({ alias: this.actor.name }),
      });
    });

    html.find('.item-upgrade').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // if (item.data.data.ranks) {
      //   const rank = item.data.data.ranks + 1;
      // V10 changes
      const item = this.actor.items.get(li.data('itemId'));
      if (item.system.ranks) {
        const rank = item.system.ranks + 1;
		// change End
        item.update({
          // 'data.ranks': rank,
          // V10 changes
          'system.ranks': rank,
		      // change End
        });
      // } else if (item.data.data.circle) {
      //   const circle = item.data.data.circle + 1;
      // V10 changes
      } else if (item.system.circle) {
        const circle = item.system.circle + 1;
		  // change End
        item.update({
          // 'data.circle': circle,
          // V10 changes
          'system.circle': circle,
		      // change End
        });
      }
    });

    html.find('.item-downgrade').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // if (item.data.data.ranks) {
      //   const rank = item.data.data.ranks - 1;
      // V10 changes
      const item = this.actor.items.get(li.data('itemId'));
      if (item.system.ranks) {
        const rank = item.system.ranks - 1;
		  // change End
        item.update({
          // 'data.ranks': rank,
          // V10 changes
          'system.ranks': rank,
		      // change End
        });
      // } else if (item.data.data.circle !== undefined) {
      //   const circle = item.data.data.circle - 1;
      // V10 changes
      } else if (item.system.circle !== undefined) {
        const circle = item.system.circle - 1;
		  // change End
        item.update({
          // 'data.circle': circle,
          // V10 changes
          'system.circle': circle,
		      // change End
        });
      }
    });

    html.find('.show-hidden').click((event) => this._showItemDescription(event));
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find('.spell-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      //let itemID = li.data('itemId');
      // V10 changes
      let itemID = li.data('itemId');
		  // change End
      const spell = this.actor.items.get(itemID);
      this.actor.castSpell(spell);
    });
  }
  async confirmationBox() {
    return await new Promise((resolve) => {
      new Dialog({
        title: `Confirm Delete`,
        content: `Are You Sure?

              `,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                result: true,
              });
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
            callback: (html) => {
              resolve({
                result: false,
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });
  }

  /** @override */
  async _onDropItemCreate(itemData) {
    //console.debug("[EARTHDAWN]\tIn _onDropItemCreate");

    let itemType = itemData.type;

    console.debug(`[EARTHDAWN]\tType of dropped item: ${itemType}`);

    // Increment the number of a discipline circle  or talent/skill rank of a character instead of creating a new item
    if (itemType === 'discipline' || itemType === 'talent' || itemType === 'skill') {
      console.debug(`[EARTHDAWN]\tDropped ${itemType} item`);

      const levelAttributeName = itemType === 'discipline' ? 'circle' : 'ranks';
      // const droppedItem = this.actor.itemTypes[itemType].find((c) => c.name === itemData.name);
      // let priorLevel = Number.parseInt(droppedItem?.data.data[levelAttributeName]) || 0;
      // V10 changes
      const droppedItem = this.actor.itemTypes[itemType].find((c) => c.name === itemData.name);
      let priorLevel = Number.parseInt(droppedItem?.system[levelAttributeName]) || 0;
		  // change End
      if (!!droppedItem) {
        console.debug(`[EARTHDAWN]\tFound corresponding ${itemType} already existing on actor`);
        console.debug(`[EARTHDAWN]\tCurrent ${itemType} level: ${priorLevel}`);

        const next = priorLevel + 1;
        if (next > priorLevel) {
          // return droppedItem.update({ [`data.${levelAttributeName}`]: String(next) });
          // V10 changes
          return droppedItem.update({ [`system.${levelAttributeName}`]: String(next) });
		      // change End
        }
      } else {
        console.debug('[EARTHDAWN]\tNo existing item found in actor. Add new item.');
        // Default drop handling if levels were not added
        return super._onDropItemCreate(itemData);
      }
    } else if (itemType === 'mask') {
      return await this._onDropItemMask(itemData);
    } else {
      console.debug('[EARTHDAWN]\tNo corresponding item type routine found. Add new item.');
      // Default drop handling if levels were not added
      return super._onDropItemCreate(itemData);
    }
  }

  async _onDropItemMask(itemData) {
    if (this.actor.type !== 'creature') {
      ui.notifications.warn(game.i18n.format('earthdawn.m.maskActorTypeWrong', { actorType: this.actor.type }));
      return null;
    }


    // let creatureData = this.actor.data.data;
    // let maskData = itemData.data;
    // V10 changes
    let creatureData = this.actor.system;
    let maskData = itemData.system;
		// change End
    const challengeIncrease = parseInt(maskData['challenge']);
    const isChallengeIncrease = challengeIncrease >= 0;
    if (isChallengeIncrease) {
      ChatMessage.create({
        type: CONST.CHAT_MESSAGE_TYPES.WHISPER,
        user: game.user._id,
        flavor: game.i18n.localize('earthdawn.s.systemInfo'),
        content: `<div style='background-color:yellow'>${game.i18n.localize('earthdawn.m.maskChallengeIncreaseWarn')}</div>`,
        whisper: game.user._id,
      });
      ui.notifications.warn(game.i18n.localize('earthdawn.c.chatWarning'));
    }

    creatureData['dexterityStep'] = this._addMaskValue(creatureData['dexterityStep'], maskData['dexterityStep'], 'number');
    creatureData['strengthStep'] = this._addMaskValue(creatureData['strengthStep'], maskData['strengthStep'], 'number');
    creatureData['toughnessStep'] = this._addMaskValue(creatureData['toughnessStep'], maskData['toughnessStep'], 'number');
    creatureData['perceptionStep'] = this._addMaskValue(creatureData['perceptionStep'], maskData['perceptionStep'], 'number');
    creatureData['willpowerStep'] = this._addMaskValue(creatureData['willpowerStep'], maskData['willpowerStep'], 'number');
    creatureData['initiativeStep'] = this._addMaskValue(creatureData['initiativeStep'], maskData['initiativeStep'], 'number');
    creatureData['movement'] = this._addMaskValue(creatureData['movement'], maskData['movement'], 'number');
    creatureData['otherMovement'] = this._addMaskValue(creatureData['otherMovement'], maskData['otherMovement'], 'number');
    creatureData['physicaldefense'] = this._addMaskValue(creatureData['physicaldefense'], maskData['physicaldefense'], 'number');
    creatureData['mysticdefense'] = this._addMaskValue(creatureData['mysticdefense'], maskData['mysticdefense'], 'number');
    creatureData['socialdefense'] = this._addMaskValue(creatureData['socialdefense'], maskData['socialdefense'], 'number');
    creatureData['physicalarmor'] = this._addMaskValue(creatureData['physicalarmor'], maskData['physicalarmor'], 'number');
    creatureData['mysticarmor'] = this._addMaskValue(creatureData['mysticarmor'], maskData['mysticarmor'], 'number');
    creatureData['knockdown'] = this._addMaskValue(creatureData['knockdown'], maskData['knockdown'], 'number');
    creatureData['recoverytests'] = this._addMaskValue(creatureData['recoverytests'], maskData['recoverytests'], 'number');
    creatureData['deathThreshold'] = this._addMaskValue(creatureData['deathThreshold'], maskData['deathThreshold'], 'number');
    creatureData['unconsciousThreshold'] = this._addMaskValue(
      creatureData['unconsciousThreshold'],
      maskData['unconsciousThreshold'],
      'number',
    );
    creatureData['woundThreshold'] = this._addMaskValue(creatureData['woundThreshold'], maskData['woundThreshold'], 'number');
    creatureData['attacks'] = this._addMaskValue(creatureData['attacks'], maskData['attacks'], 'number');
    creatureData['challenge'] = `${creatureData['challenge']} ${isChallengeIncrease ? '+' : ''}${
      maskData['challenge']
    } ${game.i18n.localize('earthdawn.f.fromMask')}`;
    creatureData['description'] = this._addMaskValue(creatureData['description'], maskData['description'], 'string');

    let actorClone = await this.actor.clone({}, { save: true });

    try {
      // actorClone.update({ data: creatureData, name: `${this.actor.data.name} (${itemData.name})` });
      // V10 changes
      actorClone.update({ data: creatureData, name: `${this.actor.system.name} (${itemData.name})` });
		  // change End

      // update attack steps
      this._updateStepsFromMask(actorClone.items, 'attackstep', maskData['attackStep']);
      // update damage steps
      this._updateStepsFromMask(actorClone.items, 'damagestep', maskData['damageStep']);

      // update powers
      await this._updatePowersFromMask(actorClone, maskData['powers']);

      actorClone.sheet.render(true);
    } catch (e) {
      console.error(e);
      await actorClone.delete();
      ui.notifications.error(game.i18n.localize('earthdawn.e.errorMaskDrop'));
    }
  }

  _showItemDescription(event) {
    event.preventDefault();
    const toggler = $(event.currentTarget);
    const item = toggler.parents('.showHidden');
    const description = item.find('.hidden-tab');

    $(description).slideToggle(function () {
      $(this).toggleClass('open');
    });
  }

  _addMaskValue(oldValue, valueUpdate, additionType) {
    if (oldValue == null) {
      return null;
    }
    let newValue = 0;

    switch (additionType) {
      case 'number':
        newValue = parseInt(oldValue) + parseInt(valueUpdate);
        break;
      case 'string':
        newValue = `${String(oldValue)}\n${String(valueUpdate)}`;
        break;
    }

    // cast to original data type
    return oldValue.constructor(newValue);
  }

  _updateStepsFromMask(creatureItems, stepType, addedValue) {
    creatureItems
      //.filter((e) => e.data.data.hasOwnProperty(stepType))
      // V10 changes
      .filter((e) => e.system.hasOwnProperty(stepType))
		  // change End
      .map((e) => {
        //const oldValue = e.data.data[stepType];
        // V10 changes
        const oldValue = e.system[stepType];
		    // change End
        if (oldValue != 0) {
          // no type comparison since it might be string
          // e.update({ [`data.${stepType}`]: e.data.data[stepType] + addedValue });
          // V10 changes
          e.update({ [`system.${stepType}`]: e.system[stepType] + addedValue });
		      // change End
        }
      });
  }

  async _updatePowersFromMask(newActor, maskPowers) {
    const htmlTagRegex = /<\/?[^>]+(>|$)/g;
    //const textInSquareBracketsRegex = /\[([^\]]*)\]/;
    const itemLinkRegex = /(@Compendium|@Item)\[([^\]]*)\]/;
    //const textInCurlyBracketsRegex = /\{([^}]*)\}/;

    for (const p of maskPowers.replace(htmlTagRegex, '').split(',')) {
      if (!p) {
        continue;
      }

      let linkMatches = p.match(itemLinkRegex);
      if (linkMatches.length <= 1) {
        ui.notifications.warn(game.i18n.format('earthdawn.m.maskPowerNotParsable', { maskPower: 'p' }));
        continue;
      }

      let maskPower = null;

      if (linkMatches[1].toLowerCase().includes('compendium')) {
        let compendiumParts = linkMatches[2].split('.');
        const maskPowerCompID = compendiumParts.pop();
        const compendiumID = compendiumParts.join('.');

        maskPower = await game.packs.get(compendiumID).getDocument(maskPowerCompID);
      } else if (linkMatches[1].toLowerCase().includes('item')) {
        maskPower = game.items.get(linkMatches[2]);
      } else {
        ui.notifications.warn(game.i18n.format('earthdawn.m.maskPowerNotParsable', { maskPower: 'p' }));
        continue;
      }

      if (maskPower == null) {
        // only == instead of ===, this way "undefined" is catched as well
        ui.notifications.warn('Could not find mask power in available companions.');
        console.info(`[EARTHDAWN]\tCould not find mask power in available companions from link text "${p}"`);
      } else {
        this._addMaskPower(newActor, maskPower);
      }
    }
  }

  _addMaskPower(newActor, maskPower) {
    const powerTitleSeparatedRegex = /(^[^\(]+)\(?([^\)]*)/;
    let powerTitle = maskPower.name;

    let powerTitleSeparated = maskPower.name.match(powerTitleSeparatedRegex);
    let powerName = powerTitleSeparated[1].trim();

    const hasRank = powerTitle.includes('(');
    let existingPowers = newActor.items.filter(
      (item) => item.name.match(powerTitleSeparatedRegex)[1].trim().toLowerCase() === powerName.toLowerCase(),
    );
    const alreadyExists = existingPowers.length > 0;

    if (alreadyExists) {
      //handle Item
      let existingPower = existingPowers[0];
      if (hasRank) {
        let existingTitleSeparated = existingPower.name.match(powerTitleSeparatedRegex);
        let existingRank = parseInt(existingTitleSeparated[2]);
        let maskRank = parseInt(powerTitleSeparated[2]);

        if (isNaN(existingRank) || isNaN(maskRank)) {
          // Rank is not a number but some string, e.g. combination of Attribute + Circle
          // We'll add both items then
          console.log(`[EARTHDAWN]\tAdded both powers from mask: ${existingPower.name} and ${maskPower.name}.`);
          newActor.createEmbeddedDocuments('Item', [maskPower.toObject()]);
        } else {
          existingPower.update({
            name: `${powerName.trim()} (${existingRank > maskRank ? existingRank + 2 : maskRank + 2})`,
          });
        }
      } else {
        // Do nothing.
        // The power to add already exists and has no associated rank.
        // Therefore, it does not have to be changed.
        // Attack and Damage Step are already taken care of.
      }
    } else {
      //add Item
      newActor.createEmbeddedDocuments('Item', [maskPower.toObject()]);
    }

    //console.debug(`[EARTHDAWN] Mask Power | Could not parse mask power with name "${powerTitle}"`);
  }
}
