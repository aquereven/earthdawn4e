import Earthdawn4eActorSheet from './earthdawn-4e-actor-sheet.js';

export default class earthdawn4eNPCSheet extends Earthdawn4eActorSheet {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 520,
      height: 620,
      classes: ['earthdawn', 'sheet', 'actor', 'creature'],
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'main',
        },
      ],
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/actors/sheets/npc-sheet.hbs`;
  }

  getData() {
    const sheetData = super.getData();
    // const itemData = data.data;

    // data.item = itemData;
    // data.data = itemData.data;
    // data.attacks = data.items.filter(function (item) {
    // V10 changes
    //const itemData = system;

    //system.item = itemData;
    //system = itemData.system;
    sheetData.attacks = sheetData.items.filter(function (item) {
		// change End
      return item.type === 'attack';
    });

    console.log('[EARTHDAWN] NPC Data', sheetData);

    return sheetData;
  }

  activateListeners(html) {
    super.activateListeners(html);
    this.baseListeners(html);

    html.find('.step-roll').click((ev) => {
      const att = $(ev.currentTarget).attr('data-att');
      const name = $(ev.currentTarget).attr('data-name');
      let inputs = { attribute: att, name: name };
      this.actor.rollPrep(inputs);
    });

    html.find('.effectTest').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const weapon = this.actor.items.get(li.data('itemId'));
      // this.actor.NPCDamage(this, weapon.data.data.damagestep, 0);
    // V10 changes
    const weapon = this.actor.items.get(li.data('itemId'));
    this.actor.NPCDamage(this, weapon.system.damagestep, 0);
		// change End
    });

    html.find('.ability-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      // const item = this.actor.items.get(li.data('itemId'));
      // const att = item.data.data.attribute;
      // const ranks = item.data.data.ranks;
      // const strain = item.data.data.strain;
      // const type = item.data.type;
      // const karmaAllowed = item.data.data.karma;
    // V10 changes
    const item = this.actor.items.get(li.data('itemId'));
    const att = item.system.attribute;
    const ranks = item.system.ranks;
    const strain = item.system.strain;
    const type = item.system.type;
    const karmaAllowed = item.system.karma;
		// change End
    
      const parameters = {
        talent: item.name,
        attribute: att,
        ranks: ranks,
        strain: strain,
        type: type,
        karmaallowed: karmaAllowed,
        //karmarequested: this.actor.data.data.usekarma,
        // V10 changes
        karmarequested: this.actor.system.usekarma,
		    // change End
      };
      console.log(parameters)
      this.actor.attributetest(parameters);
    });
  }
}
